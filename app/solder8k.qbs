import qbs 1.0

    Product {
    type: "application"
    Depends { name:"cpp" }
    property string drivers: "source_c/Drivers/"
    property string vendor: "STM"
    property string model: "STM32F3"
    property string cortex: "M4"
    cpp.defines: ["TOOLCHAIN_GCC_CW=1"]
    cpp.positionIndependentCode: false
    cpp.debugInformation: true
    cpp.warningLevel: "default"

    cpp.commonCompilerFlags: [
        "-mcpu=cortex-m4",
        "-mfpu=fpv4-sp-d16",
        "-mfloat-abi=hard",
        "-mthumb",
        "-Wall",
        "-ffunction-sections",
        "-fdata-sections",
        "-fno-inline",


    ]

    cpp.linkerFlags:[
        "-mthumb",
        "-mcpu=cortex-m4",
        "-mfloat-abi=hard",
        "-mfpu=fpv4-sp-d16",
        "--specs=nano.specs",
        "-Wl,--start-group",
        "-Wl,--gc-sections",
        "-Wl,-Map,somefilemap.map",
        "-T",
       path + "/STM32F373VCTx_FLASH.ld",
        "-lnosys",

    ]

    Properties {
        condition: qbs.buildVariant === "debug"

        cpp.commonCompilerFlags: outer.concat (
        "-ggdb",
        "-O0"
        )
        cpp.debugInformation: true
        cpp.optimization: "none"
        cpp.linkerFlags: outer.concat (
            "-ggdb"
        )
    }

    Properties {
        condition: qbs.buildVariant === "release"

        cpp.commonCompilerFlags: outer.concat(
             "-O1",
             "-ggdb"
        )
        cpp.debugInformation: true
        cpp.optimization: "small"
        cpp.linkerFlags: outer.concat (
            "-ggdb"
        )
    }


    cpp.includePaths: [
        drivers + "CMSIS/Device/ST/STM32F3xx/Include",
        drivers + "CMSIS/Include",
        drivers + "STM32F3xx_HAL_Driver/Inc",
        drivers + "STM32F3xx_HAL_Driver/Inc/Legacy",
        "source_c/Src",
        "source_cpp",
        "source_cpp/ionscreens",
        "source_cpp/components",
        "source_cpp/ionscreens",
        "source_cpp/oxiscreens",
        "source_cpp/condscreens",
        "source_cpp/comscreens",
        "source_cpp/ionMeasure"
    ]
    files: [

            drivers + "CMSIS/Device/ST/STM32F3xx/Include/*.h",
            drivers + "STM32F3xx_HAL_Driver/Src/*.c",
            drivers + "STM32F3xx_HAL_Driver/Inc/*.h",
            "source_c/startup/*.S",
            "source_c/Src/*.c",
            "source_c/Src/*.h",
            "source_cpp/main.cpp",
            "source_cpp/main.h",
            "source_cpp/ionscreens/*.cpp",
            "source_cpp/components/*.cpp",
            "source_cpp/ionscreens/*.cpp",
            "source_cpp/oxiscreens/*.cpp",
            "source_cpp/condscreens/*.cpp",
            "source_cpp/comscreens/*.cpp",
            "source_cpp/ionMeasure/*.cpp",
            "source_cpp/ionscreens/*.h",
            "source_cpp/components/*.h",
            "source_cpp/ionscreens/*.h",
            "source_cpp/oxiscreens/*.h",
            "source_cpp/condscreens/*.h",
            "source_cpp/comscreens/*.h",
            "source_cpp/ionMeasure/*.h",
        ]
    Properties {
        condition: qbs.buildVariant === "debug"
        cpp.defines: outer.concat(["DEBUG=1"])
    }
    Group {
        qbs.install: true
        fileTagsFilter: "application"
    }
}
/*
"source_cpp/basescreen.cpp",
"source_cpp/basescreen.h",
"source_cpp/button.cpp",
"source_cpp/button.h",
"source_cpp/container.cpp",
"source_cpp/container.h",
"source_cpp/dateedit.cpp",
"source_cpp/dateedit.h",
"source_cpp/datetimescreen.cpp",
"source_cpp/datetimescreen.h",
"source_cpp/hlinecontrol.cpp",
"source_cpp/hlinecontrol.h",
"source_cpp/label.cpp",
"source_cpp/label.h",
"source_cpp/scrmanage.cpp",
"source_cpp/scrmanage.h",
"source_cpp/table.cpp",
"source_cpp/table.h",
"source_cpp/control.cpp",
"source_cpp/control.h",
"source_cpp/main.cpp",
"source_cpp/main.h",
"source_cpp/ionselectscreen.cpp",
"source_cpp/ionselectscreen.h",
"source_cpp/digbutton.cpp",
"source_cpp/digbutton.h",
"source_cpp/timeedit.cpp",
"source_cpp/timeedit.h",
"source_cpp/valedit.cpp",
"source_cpp/valedit.h",
"source_cpp/vscrollbar.cpp",
"source_cpp/vscrollbar.h",*/
