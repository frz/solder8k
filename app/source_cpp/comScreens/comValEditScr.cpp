#include "comValEditScr.h"

//#include "ionSettingMin.h"
#include "label.h"
#include "valedit.h"
#include "scrmanage.h"
extern ScreenManager screenManager;

bool ComValEditActionEnter(BaseScreen * parent)
{
    ComValEditScr *tmp = (ComValEditScr *)(parent);
    if (tmp->fvaluep != NULL) *(tmp->fvaluep)=tmp->valEdit->getValue();
    screenManager.closeCurrentScreen();
    screenManager.openPrevScreen();
    return true;
}

ComValEditScr::ComValEditScr(char* title,char* units,
                             uint8_t ilen, uint8_t flen, bool sign)
{
    this->setRect(0,0,128,64);

    Label * titletxt = new Label(0,0,128,12,0,Txt_Align_Center,title);
    titletxt->visible=true;

    valEdit = new ValEdit(3,20,0,24,BIG_FONT,ilen,flen,sign);
    valEdit->setVisible(true);
    Label * unitstxt = new Label(valEdit->rect.x+valEdit->rect.w+4,
                                 34,30,10,SML_FONT,Txt_Align_Left,units);
    unitstxt->setVisible(true);

    this->addElem(titletxt);
    this->addElem(unitstxt);
    this->addElem(valEdit);
    this->setSelected(true);
    fvaluep=NULL;
    //valEdit->setValue(*value);
//*value=valEdit->getValue();
    this->actionEnter=ComValEditActionEnter;

}
