#include "devSelect.h"
//#include "ionSelect.h"
//#include "oxiSelect.h"
//#include "condSelect.h"
#include "button.h"
#include "label.h"
#include "table.h"
#include "basescreen.h"
#include "scrmanage.h"
#include "malloc.h"

const char * devSelScreenBtnNames[3]=
{
  "�������",
  "������������",
  "�����������"
};

extern ScreenManager screenManager;

bool funcBtnIonSelect(BaseScreen *parent)
{
    int16_t a[2]={2,-1};
    screenManager.closeCurrentScreen(true);
  //  screenManager.currentScreen=openIonSelectScr(a);
    return true;
}
bool funcBtnOxiSelect(BaseScreen *parent)
{
    int16_t a[2]={2,-1};
    screenManager.closeCurrentScreen(true);
 //   screenManager.currentScreen=openOxiSelectScr(a);
    return true;
}
bool funcBtnCondSelect(BaseScreen *parent)
{
    int16_t a[2]={2,-1};
    screenManager.closeCurrentScreen(true);
  //  screenManager.currentScreen=openCondSelectScr(a);
    return true;
}

DevSelectScr::DevSelectScr(int16_t * addrLine)
{
    this->setRect(0,0,128,64);
    Label * titleTxt = new Label(0,0,128,12,0,Txt_Align_Left,(char*)"����� �������");
    //Label * sizeTxt = new Label(0,0,128,12,0,Txt_Align_Center);

    titleTxt->visible=true;

    Button * button[3];
    Table * table=new Table(0,13,this->rect.w,this->rect.h-12);
    table->setTableSize(1,3);
    for(int k=0;k<3;k++)
    {
        button[k] = new Button(0,0,100,12,0,Txt_Align_Center,(char*)devSelScreenBtnNames[k]);
        table->addElem(button[k]);
    }
    //table->addElem(sizeTxt);
    this->myLabel=titleTxt;
    button[0]->setAction(funcBtnIonSelect,this);
    button[1]->setAction(funcBtnOxiSelect,this);
    button[2]->setAction(funcBtnCondSelect,this);
    actionCancel=NULL;
    this->addElem(titleTxt);
    this->addElem(table);
    this->setSelected(true,addrLine);

}

BaseScreen * openDevSelectScr(int16_t *addrLine)
{
    BaseScreen * tmp = new DevSelectScr(addrLine);
    tmp->openFuncPtr=&openDevSelectScr;
    return tmp;
}
