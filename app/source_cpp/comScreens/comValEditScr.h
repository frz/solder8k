#ifndef COMVALEDITSCR_H
#define COMVALEDITSCR_H


#include "basescreen.h"
#include "valedit.h"

class ComValEditScr: public BaseScreen
{

public:
    float * fvaluep;
    ValEdit * valEdit;
    void setEditValuePtr(float * fvalueptr) {fvaluep = fvalueptr;}
    ComValEditScr(char* title,char* units,
                  uint8_t ilen=2, uint8_t flen=2, bool sign=true);
};

#endif // COMVALEDITSCR_H
