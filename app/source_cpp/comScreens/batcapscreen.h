#ifndef BATSCREEN_H
#define BATSCREEN_H

#include "valedit.h"
#include "basescreen.h"
#include "label.h"

class BatCapScreen:public BaseScreen
{
public:
    float * fvaluep;
    float voltage;
    float current;
    float capacity;
    float resistor[4];
    float worktime;
    uint8_t chanNum;

    ValEdit * valEdit;
    void setEditValuePtr(float * fvalueptr) {fvaluep = fvalueptr;}
    BatCapScreen();
    void proc();
    float errT,intErrT;
    Label *cap;
    Label *power;
    Label *v;

};

BaseScreen * openBatCapScr(int16_t *addrLine=0);
#endif // SOLDERSCREEN_H
