#include "BatCapscreen.h"
//#include "ionSettingMin.h"
#include "label.h"
#include "button.h"
#include "comDateTime.h"


#include "valedit.h"
#include "scrmanage.h"
#include "sdadc.h"
#include "rtc.h"
#include "datetimelabel.h"
#include "timers.h"
extern int32_t adc1Data[SDADC1_CHANNAL_COUNT];
extern ScreenManager screenManager;
extern RTime currentTime;
extern RDate currentDate;
extern uint32_t timerPWM;
extern TIM_HandleTypeDef tim12Handle;


BatCapScreen::BatCapScreen()
{
    voltage=0;
    current=0;
    capacity=0;
    chanNum=0;
    resistor[1]=1.174;
    resistor[2]=5.05;
    resistor[3]=10.14;//+0.06ohm
    worktime=0;

    this->setRect(0,0,128,64);
    Button * settings = new Button(0,0,12,12,0,Txt_Align_Center,(char*)"*");
    settings->visible=true;

    Label * titletxt = new Label(20,0,100,12,0,Txt_Align_Center,(char*)"���. �������");
    titletxt->visible=true;

    Label * txt1 = new Label(0,15,35,12,0,Txt_Align_Center,(char*)"U min:");
    txt1->visible=true;

    valEdit = new ValEdit(35,15,0,12,SML_FONT,2,2,false);
    valEdit->setVisible(true);
    Label * txt2 = new Label(70,15,20,12,0,Txt_Align_Center,(char*)"U=");
    txt2->visible=true;

    //Label * unitstxt1 = new Label(valEdit->rect.x+valEdit->rect.w+4,
    //                             28,50,10,SML_FONT,Txt_Align_Left,(char*)"\x10\�");



    v = new Label(85,15,35,12,SML_FONT,Txt_Align_Right);
    v->setDigCount(4);
    v->setValue(&voltage);

    Label * txt3 = new Label(0,28,35,12,0,Txt_Align_Center,(char*)"C,mAh:");
    txt3->visible=true;
    cap = new Label(30,28,35,12,SML_FONT,Txt_Align_Right);
    cap->setDigCount(4);
    cap->setValue(&capacity);

    Label * txt4 = new Label(70,28,20,12,0,Txt_Align_Center,(char*)"I=");
    txt4->visible=true;

    Label * c = new Label(85,28,35,12,SML_FONT,Txt_Align_Right);
    c->setDigCount(4);
    c->setValue(&current);
    Button * R1 = new Button(2,51,20,12,0,Txt_Align_Center,(char*)"R1");
    R1->visible=true;
    Button * R2 = new Button(50,51,20,12,0,Txt_Align_Center,(char*)"R2");
    R2->visible=true;
    Button * R3 = new Button(100,51,20,12,0,Txt_Align_Center,(char*)"R3");
    R3->visible=true;
    R1->setAction(
                [](BaseScreen * screen)->bool
    {
       ((BatCapScreen*)screen)->chanNum=1;
        HAL_GPIO_WritePin(GPIOE,GPIO_PIN_8,GPIO_PIN_RESET);
        HAL_GPIO_WritePin(GPIOE,GPIO_PIN_7,GPIO_PIN_RESET);
          HAL_GPIO_WritePin(GPIOE,GPIO_PIN_9,GPIO_PIN_SET);
       return true;
    },this);
    R2->setAction(
                [](BaseScreen * screen)->bool
    {
       ((BatCapScreen*)screen)->chanNum=2;
        HAL_GPIO_WritePin(GPIOE,GPIO_PIN_7,GPIO_PIN_SET);
        HAL_GPIO_WritePin(GPIOE,GPIO_PIN_8,GPIO_PIN_SET);
        HAL_GPIO_WritePin(GPIOE,GPIO_PIN_9,GPIO_PIN_RESET);

        return true;
    },this);
    R3->setAction(
                [](BaseScreen * screen)->bool
    {
       ((BatCapScreen*)screen)->chanNum=3;
        HAL_GPIO_WritePin(GPIOE,GPIO_PIN_7,GPIO_PIN_SET);
        HAL_GPIO_WritePin(GPIOE,GPIO_PIN_8,GPIO_PIN_RESET);
        HAL_GPIO_WritePin(GPIOE,GPIO_PIN_9,GPIO_PIN_RESET);
        return true;
    },this);

    //Label * unitstxt2 = new Label(100,44,24,12,SML_FONT,Txt_Align_Left,(char*)"\x10\C");


    //power = new Label(64,56,28,12,SML_FONT,Txt_Align_Right);
    //power->setDigCount(3);
    //Label * unitstxt3 = new Label(100,56,12,12,SML_FONT,Txt_Align_Left,(char*)"%");

    //settings->setAction(funcBtnDateTime,this);
    addElem(settings);
    addElem(titletxt);
    addElem(txt1);
    addElem(txt2);
    addElem(txt3);
    addElem(txt4);
    //addElem(unitstxt1);
    //addElem(unitstxt2);
    //addElem(unitstxt3);
    addElem(v);
    addElem(c);
    addElem(cap);

   // addElem(power);
    addElem(valEdit);
    addElem(R1);
    addElem(R2);
    addElem(R3);


   // int16_t addr[2]={7,-1};
    this->setSelected(true);
    fvaluep=NULL;
    errT=0;
    intErrT=0;
    this->actionCancel=[](BaseScreen * screen)->bool
                       {
                        BatCapScreen * scr=(BatCapScreen*)screen;
                          scr->valEdit->setValue(0);
                        HAL_GPIO_WritePin(GPIOE,GPIO_PIN_9,GPIO_PIN_RESET);
                        HAL_GPIO_WritePin(GPIOE,GPIO_PIN_8,GPIO_PIN_RESET);
                        HAL_GPIO_WritePin(GPIOE,GPIO_PIN_7,GPIO_PIN_RESET);
                        scr->current=0;
                        scr->capacity=0;
                        scr->chanNum=0;
                        return true;
                       };
}

BaseScreen * openBatCapScr(int16_t *addrLine)
{
    BatCapScreen * screen = new BatCapScreen();
    screen->openFuncPtr=openBatCapScr;
    return screen;
}

void BatCapScreen::proc()
{
const float k=0.5/3600.0;
if (timerPWM>=1)
{

//v->setValue(adc1Data[2]*kU);
voltage=SDADC_getV2();
if (chanNum>0)
{
    current=voltage/resistor[chanNum]*1000;
    worktime+=0.5;
    capacity+=current*k;
    if (voltage<valEdit->getValue()) chanNum=0;

} else
{
    HAL_GPIO_WritePin(GPIOE,GPIO_PIN_9,GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOE,GPIO_PIN_8,GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOE,GPIO_PIN_7,GPIO_PIN_RESET);
    current=0;
}
timerPWM=0;
}

}
