#ifndef DATETIMESCREEN_H
#define DATETIMESCREEN_H

#include "basescreen.h"
#include "timeedit.h"
#include "dateedit.h"


class ComDateTimeScr:public BaseScreen
{

public:
    TimeEdit * timeEdit;
    DateEdit * dateEdit;
    ComDateTimeScr(int16_t *addrLine = 0);

};

BaseScreen * openDateTimeScr(int16_t *addrLine=0);
bool funcBtnDateTime(BaseScreen*);
#endif // DATETIMESCREEN_H
