#ifndef DEVSELECTSCREEN_H
#define DEVSELECTSCREEN_H


#include "basescreen.h"
#include "label.h"


class DevSelectScr:public BaseScreen
{
    Label * myLabel;
public:
    DevSelectScr(int16_t * addrLine = 0);
};

BaseScreen * openDevSelectScr(int16_t *addrLine = 0);

#endif // DEVSELECTSCREEN_H
