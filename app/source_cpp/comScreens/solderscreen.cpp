#include "solderscreen.h"
//#include "ionSettingMin.h"
#include "label.h"
#include "button.h"
#include "comDateTime.h"


#include "valedit.h"
#include "scrmanage.h"
#include "sdadc.h"
#include "rtc.h"
#include "datetimelabel.h"
#include "timers.h"
#include "batcapscreen.h"

extern int32_t adc1Data[SDADC1_CHANNAL_COUNT];
extern ScreenManager screenManager;
extern RTime currentTime;
extern RDate currentDate;
extern uint32_t timerPWM;
extern TIM_HandleTypeDef tim12Handle;

bool funcBtnBatCap(BaseScreen*)
{
    screenManager.closeCurrentScreen(true);
    screenManager.currentScreen=openBatCapScr();
    return true;
}


SolderScreen::SolderScreen()
{
    this->setRect(0,0,128,64);
    Button * settings = new Button(0,0,12,12,0,Txt_Align_Center,(char*)"*");
    settings->visible=true;

    Button * batcap= new Button(15,0,12,12,0,Txt_Align_Center,(char*)"B");
    batcap->visible=true;

    Label * titletxt = new Label(25,0,100,12,0,Txt_Align_Center,(char*)"����� 1.0");
    titletxt->visible=true;

    valEdit = new ValEdit(3,20,0,16,MED_FONT,3,0,false);
    valEdit->setVisible(true);
    Label * unitstxt1 = new Label(valEdit->rect.x+valEdit->rect.w+4,
                                 24,30,10,SML_FONT,Txt_Align_Left,(char*)"\x10\C");
    temp = new Label(64,20,28,12,MED_FONT,Txt_Align_Right);
    temp->setDigCount(4);

   // v = new Label(2,36,50,12,MED_FONT,Txt_Align_Right);
   // v->setDigCount(5);

    Label * unitstxt2 = new Label(100,24,24,12,SML_FONT,Txt_Align_Left,(char*)"\x10\C");


    power = new Label(64,36,28,12,SML_FONT,Txt_Align_Right);
    power->setDigCount(3);
    Label * unitstxt3 = new Label(100,36,12,12,SML_FONT,Txt_Align_Left,(char*)"%");

    DateTimeLabel * dtl = new DateTimeLabel(5,51,SML_FONT);
    dtl->setDateTimePtr(&currentTime,&currentDate);
    settings->setAction(funcBtnDateTime,this);
    batcap->setAction(funcBtnBatCap,this);
    addElem(settings);
    addElem(batcap);
    addElem(titletxt);
    addElem(unitstxt1);
    addElem(unitstxt2);
    addElem(unitstxt3);
    addElem(temp);
    //addElem(v);
    addElem(power);
    addElem(valEdit);
    addElem(dtl);
    //int16_t addr[2]={7,-1};
    this->setSelected(true);
    fvaluep=NULL;
    errT=0;
    intErrT=0;
   this->actionCancel=[](BaseScreen * screen)->bool
                       {
                          ((SolderScreen*)screen)->valEdit->setValue(0);
                          return true;
                       };
}

BaseScreen * openSolderScr(int16_t *addrLine)
{
    SolderScreen * screen = new SolderScreen();
    screen->openFuncPtr=openSolderScr;
    return screen;
}

void SolderScreen::proc()
{
  //  float kU=1257.0/3040.0;
if (timerPWM>=1)
{

temp->setValue(((float)adc1Data[1])/(float)60.0);

errT=(valEdit->getValue()*10-adc1Data[1]/6);
intErrT+=errT/5;
if (intErrT>9000) intErrT=9000;
if (intErrT<-1000) intErrT=-1000;

float pwmf;
pwmf=intErrT+errT*5;
int32_t pwm=0;
if (pwmf > 0)
pwm=(int16_t)pwmf;
if (pwm>10000) pwm=10000;
power->setValue(pwm/100);
//v->setValue(adc1Data[2]*kU);
__HAL_TIM_SET_COMPARE(&tim12Handle,TIM_CHANNEL_1,pwm);
timerPWM=0;
}

}
