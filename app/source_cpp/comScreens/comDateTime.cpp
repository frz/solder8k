#include "comDateTime.h"
#include "label.h"
#include "dateedit.h"
#include "timeedit.h"
#include "scrmanage.h"
extern ScreenManager screenManager;

bool funcActionEnter(BaseScreen * parent)
{
    ComDateTimeScr * dt = (ComDateTimeScr*)(parent);
    RTime tmpt=dt->timeEdit->getTime();
    RDate tmpd=dt->dateEdit->getDate();
    RTC_setDateTime(&tmpt,&tmpd);
    screenManager.closeCurrentScreen();
    screenManager.openPrevScreen();
    return true;
}

bool funcBtnDateTime(BaseScreen*)
{
    screenManager.closeCurrentScreen(true);
    screenManager.currentScreen=openDateTimeScr();
    return true;
}

ComDateTimeScr::ComDateTimeScr(int16_t *addrLine)
{

    setRect(0,0,128,64);
    Label * titletxt = new Label(0,0,128,12,0,Txt_Align_Center,(char*)"��������� �������");
    titletxt->visible=true;
    Label * timetxt = new Label(0,20,40,10,0,Txt_Align_Left,(char*)"�����:");
    Label * datetxt = new Label(0,45,40,12,0,Txt_Align_Left,(char*)"����:");
    timetxt->setVisible(true);
    datetxt->setVisible(true);
    timeEdit = new TimeEdit(40,17,40,15,1);
    dateEdit = new DateEdit(40,42,60,15,1);
    timeEdit->setVisible(true);
    dateEdit->setVisible(true);

    addElem(titletxt);
    addElem(timetxt);
    addElem(timeEdit);
    addElem(datetxt);
    addElem(dateEdit);
    //setSelected(true);
    activElem=-1;
    actionEnter=funcActionEnter;
}
BaseScreen * openDateTimeScr(int16_t *addrLine)
{
    ComDateTimeScr * screen=new ComDateTimeScr(addrLine);
    screen->openFuncPtr=openDateTimeScr;
    return screen;
}


