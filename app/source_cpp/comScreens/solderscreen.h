#ifndef SOLDERSCREEN_H
#define SOLDERSCREEN_H

#include "valedit.h"
#include "basescreen.h"
#include "label.h"

class SolderScreen:public BaseScreen
{
public:
    float * fvaluep;
    ValEdit * valEdit;
    void setEditValuePtr(float * fvalueptr) {fvaluep = fvalueptr;}
    SolderScreen();
    void proc();
    float errT,intErrT;
    Label *temp;
    Label *power;
    Label *v;

};

BaseScreen * openSolderScr(int16_t *addrLine=0);
#endif // SOLDERSCREEN_H
