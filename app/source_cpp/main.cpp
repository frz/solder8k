/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  * COPYRIGHT(c) 2017 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
//#pragma GCC diagnostic warning "-Wall"
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f3xx_hal.h"
#include "dac.h"
#include "dma.h"
#include "sdadc.h"
#include "spi.h"
#include "usart.h"
#include "gpio.h"
#include "spiflash.h"
#include "lcd128x64.h"
#include "graph.h"
#include "textout.h"
#include "button.h"
#include "table.h"
#include "rtc.h"
#include "extevents.h"
#include "control.h"
#include "container.h"
#include "hlinecontrol.h"

#include "devSelect.h"
#include "solderscreen.h"
#include "datetimelabel.h"
#include "timers.h"
#include "scrmanage.h"
#include "comDateTime.h"
#include "malloc.h"


extern uint32_t keysState;
ScreenManager screenManager;
void SystemClock_Config(void);
void Error_Handler(void);


//ButtonList buttonDataList;
//-------------------------MAIN.CPP------------------------------------------
int main(void)
{


  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
 MX_DAC1_Init();
 MX_DAC2_Init();
 MX_SDADC1_Init();
 MX_SDADC2_Init();
 DMA_init();
 SPI1_Init();
 SPI3_Init();

 MX_USART1_UART_Init();
  LCD128x64_init();
  ExtEvents_init();
 RTC_init();
 int k=0;
  TIMERS_init();

  screenManager.currentScreen=openSolderScr();
  Label memsize(100,0,28,12,SML_FONT,Txt_Align_Left);

  struct mallinfo  meminf;
  memsize.setVisible(true);
  uint32_t id=0;
  SPIFlash_wakeUP();
  id=SPIFlash_readId();


  while (1)
  {
k++;
if (k>2000)
{
    k=0;
   if (keysState>0)
   {
       if (!screenManager.currentScreen->keyPressed(keysState))
       {
           switch (keysState)
           {

           case KeyEnter:
               break;
           default:
              // return false;
               break;
           }
       };
       keysState=0;
   }
screenManager.currentScreen->proc();
   screenManager.currentScreen->print();

// meminf=mallinfo();
  // memsize.print();


}


  //id= SPIFlash_readByte(0xabcdec);
//  SPIFlash_writeByte(0xabcdec,0xff);
 // id= SPIFlash_readByte(0xabcdec);
LCD128x64_writeVMem2LCD();
  }


}


/** System Clock Configuration
*/
void SystemClock_Config(void)
{

 // RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

__HAL_RCC_HSE_CONFIG(RCC_HSE_ON);
while(__HAL_RCC_GET_FLAG(RCC_FLAG_HSERDY) == RESET){};
__HAL_RCC_PLL_CONFIG(RCC_PLLSOURCE_HSE,RCC_PLL_MUL2);
__HAL_RCC_PLL_ENABLE();
/* Wait till PLL is ready */
while(__HAL_RCC_GET_FLAG(RCC_FLAG_PLLRDY)  == RESET){};

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }

/* ����� ��� ���, ������� � �� �������
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_SDADC;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
  PeriphClkInit.SdadcClockSelection = RCC_SDADCSYSCLK_DIV4;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
*/
    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

void Error_Handler(void)
{
  while(1) 
  {
  }
}

