#ifndef BASESCREEN_H
#define BASESCREEN_H
#include "container.h"

class BaseScreen : public Container
{

public:
    bool (*actionEnter)(BaseScreen *);
    bool (*actionCancel)(BaseScreen *);
    BaseScreen(uint16_t x=0,uint16_t y=0,uint16_t w=0,uint16_t h=0);
    BaseScreen * (*openFuncPtr)(int16_t * activElemAddr);
    void print(void);
    bool keyPressed(uint32_t keyState);

    //bool addElem(Control * control);
    bool moveCursorUp();
    bool moveCursorDown();
    virtual void proc(){}
    //objRect* getWorkSpace(){return &workSpace;}
};

#endif // BASESCREEN_H
