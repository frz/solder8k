#ifndef VALEDIT_H
#define VALEDIT_H
#include "control.h"
#include "stm32f3xx_hal.h"
#include "digbutton.h"

#define DIG_COUNT 7
class ValEdit : public Control
{
    DigButton digit[DIG_COUNT];
    DigButton sign;
    DigButton * activButton;
    //int8_t valueDig[DIG_COUNT];
    bool isSigned;
    int8_t valSign;
    int16_t length;
    int16_t ilength;
    int16_t flength;

    uint8_t font;
    uint16_t digWidth;
    uint16_t dotWidth;
    uint16_t dotX;
    uint16_t digHeight;
    uint16_t digNum;

//    int32_t ivalue;
void setButtonPos();
void value2Dig();

public:
    float   fvalue;
    ValEdit(int16_t x, int16_t y, int16_t w, int16_t h, uint8_t font, int16_t ilen, int16_t flen, bool sign);

    //void setDig2Button();
    void loadValue2Dig();
    void loadDig2Value();
    float getValue();
    void setValue(float val);
    bool moveCursorRight();
    bool moveCursorLeft();
    bool keyPressed(uint32_t keyState);
    void setVisible(bool visible);
    void print();
    void setSelected(bool selected,int16_t *addrLine);
//    setValue(int32_t val);

};

#endif // VALEDIT_H
