#ifndef HLINECONTROL_H
#define HLINECONTROL_H

#include "container.h"
#include "extevents.h"
#include "graph.h"

class HLineControl: public Container
{
public:
    HLineControl(int16_t x, int16_t y, int16_t w, int16_t h);
    bool moveCursorLeft();
    bool moveCursorRight();
    void setSelected(bool selected,int16_t *addrLine = 0);
    bool keyPressed(uint32_t keyState);
    void print();
};

#endif // HLINECONTROL_H
