#include "datetimelabel.h"

DateTimeLabel::DateTimeLabel(uint16_t x,uint16_t y,uint8_t font,bool fullForm)
{
    selectable=false;
    visible=true;
    setRect(x,y,0,0);
    this->font=font;
    this->fullForm=fullForm;

    dotWidth = TextOut_getStringWidth(font,(char*)".")+1;
    digWidth = TextOut_getStringWidth(font,(char*)"0")+1;
    digHeight = TextOut_getStringHeight(font);

    labelLength=digWidth*9+dotWidth*2;
    if (fullForm)
    labelLength=digWidth*13+dotWidth*4;

    if (rect.w<labelLength) rect.w=labelLength;
    if (digHeight<rect.h) rect.h=digHeight;
    pTime=NULL;
    pDate=NULL;

}

void DateTimeLabel::setDateTime(RTime time,RDate date)
{
    rTime=time;
    rDate=date;
}

void DateTimeLabel::setDateTimePtr(RTime * ptime,RDate * pdate)
{
    pTime=ptime;
    pDate=pdate;
}

void DateTimeLabel::print()
{
    uint16_t x=rect.x,y=rect.y;
    if (pTime!=NULL) rTime=*pTime;
    if (pDate!=NULL) rDate=*pDate;

    TextOut_printChar(x,y,TS_XOR,font,(rTime.hh/10)+'0');
    x+=digWidth;
    TextOut_printChar(x,y,TS_XOR,font,(rTime.hh%10)+'0');
    x+=digWidth;
    TextOut_printChar(x,y,TS_XOR,font,':');
    x+=dotWidth;

    TextOut_printChar(x,y,TS_XOR,font,(rTime.mm/10)+'0');
    x+=digWidth;
    TextOut_printChar(x,y,TS_XOR,font,(rTime.mm%10)+'0');
    x+=digWidth;
    if (fullForm)
    {
        TextOut_printChar(x,y,TS_XOR,font,':');
        x+=dotWidth;
        TextOut_printChar(x,y,TS_XOR,font,(rTime.ss/10)+'0');
        x+=digWidth;
        TextOut_printChar(x,y,TS_XOR,font,(rTime.ss%10)+'0');
        x+=digWidth;
    }

    x+=digWidth;

    TextOut_printChar(x,y,TS_XOR,font,(rDate.dd/10)+'0');
    x+=digWidth;
    TextOut_printChar(x,y,TS_XOR,font,(rDate.dd%10)+'0');
    x+=digWidth;
    TextOut_printChar(x,y,TS_XOR,font,'.');
    x+=dotWidth;

    TextOut_printChar(x,y,TS_XOR,font,(rDate.mm/10)+'0');
    x+=digWidth;
    TextOut_printChar(x,y,TS_XOR,font,(rDate.mm%10)+'0');
    x+=digWidth;
    if (fullForm)
    {
        TextOut_printChar(x,y,TS_XOR,font,'.');
        x+=dotWidth;
        TextOut_printChar(x,y,TS_XOR,font,(rDate.yy/10)+'0');
        x+=digWidth;
        TextOut_printChar(x,y,TS_XOR,font,(rDate.yy%10)+'0');
        x+=digWidth;
    }

}
