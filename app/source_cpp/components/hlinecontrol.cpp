#include "hlinecontrol.h"

HLineControl::HLineControl(int16_t x, int16_t y, int16_t w, int16_t h)
{
  setRect(x,y,w,h);
  selectable=true;
}
bool HLineControl::moveCursorLeft()
{int tmp;
    tmp = activElem - 1;
    while ((tmp>=0)&&(elem[tmp]->selectable==0)) tmp--;
    if (tmp < 0) return true;
    if (activElem>=0)
        elem[activElem]->setSelected(false);
    activElem=tmp;
    elem[activElem]->setSelected(true);
    return true;
}
bool HLineControl::moveCursorRight()
{int tmp;
    tmp = activElem + 1;
    while ((tmp<elemCount)&&(elem[tmp]->selectable==0)) tmp++;
    if (tmp >= elemCount) return true;
    if (activElem>=0)
        elem[activElem]->setSelected(false);
    activElem=tmp;
    elem[activElem]->setSelected(true);
    return true;
}

bool HLineControl::keyPressed(uint32_t keyState)
{
    if ((activElem<0)||(!elem[activElem]->keyPressed(keyState)))
    {
        switch (keyState)
        {
        case KeyRight:
            return moveCursorRight();
            break;
        case KeyLeft:
            return moveCursorLeft();
            break;
        default:
            return false;
            break;
        }
    }
    return false;
}

void HLineControl::print()
{
    Graph_rect(rect.x,rect.y,rect.w,rect.h,RS_White_White);
    for(int k=0;k<elemCount;k++)
    {
        elem[k]->print();
    }
}
void HLineControl::setSelected(bool selected,int16_t *addrLine)
{
    Container::setSelected(selected,addrLine);
}
