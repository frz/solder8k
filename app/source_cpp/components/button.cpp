#include "button.h"
#include "extevents.h"
//#include "textout.h"
Button::Button(void)
{
    rect.x=0;
    rect.y=0;
    rect.w=0;
    rect.h=0;
    this->parentScreen=NULL;
    this->actionEnter=NULL;
    title=NULL;
    font=1;
    textAlign=Txt_Align_Center;
    visible=false;
    this->selectable = true;
}

Button::Button(uint16_t x, uint16_t y, uint16_t w, uint16_t h,
               uint8_t fnt, TextAlign align, char *name)
{
    rect.x=x;
    rect.y=y;
    rect.w=w;
    rect.h=h;
    this->parentScreen=NULL;
    this->actionEnter=NULL;
    title=name;
    font=fnt;
    textAlign=align;
    visible=false;
    this->selectable = true;
}

void Button::print()
{
    int tx=0,ty=0;
    if (!visible) return;

    if ((highlight)||(isSelected()))
    {
        Graph_rect(rect.x,rect.y,rect.w,rect.h,RS_Black_Black);
        TextOut_printStringAlign(rect.x+tx,rect.y+ty,rect.w,rect.h,TS_White_Transparent,textAlign,font,title);
    }
    else
    {
        Graph_rect(rect.x,rect.y,rect.w,rect.h,RS_White_White);
        TextOut_printStringAlign(rect.x+tx,rect.y+ty,rect.w,rect.h,TS_Black_Transparent,textAlign,font,title);

    }
}
bool Button::keyPressed(uint32_t keyState)
{

    switch (keyState)
    {
    case KeyEnter:
        if ((actionEnter==NULL)||(parentScreen == NULL)) return false;
        //BaseScreen* screen = NULL;
        return (actionEnter)(parentScreen);
        break;

    default:
        return false;
        break;

    }
}
void Button::setAction(bool (*actionEnter)(BaseScreen *),BaseScreen* parent)
{
    this->actionEnter=actionEnter;
    parentScreen=parent;
}

