#ifndef CONTROL_H
#define CONTROL_H
#include "stm32f3xx_hal.h"
#include "extevents.h"

class objRect
{
public:
    int x;
    int y;
    int h;
    int w;
};

class Control
{
    bool selected;
public:
    objRect rect;
    bool visible;
    bool selectable;
    bool highlight;
    Control(int x = 0, int y = 0, int w = 0, int h = 0, bool visible = false,
            bool selected = false, bool selectable = false, bool highlight=false);

    virtual void setVisible(bool visible){this->visible=visible;}
    virtual void setSelected(bool selected, int16_t * addrLine = 0)
                    {
                        this->selected = selected && selectable;
                    }
    virtual bool getSelected(int16_t * addrLine = 0);
    virtual bool isSelected(){return selected;}
    virtual bool keyPressed(uint32_t keyState){return false;}
    virtual void print(){}
    virtual ~Control(){}
    virtual void setRect(int x, int y, int w, int h)
    {
        rect.x=x;
        rect.y=y;
        rect.w=w;
        rect.h=h;
    }
};

#endif // CONTROL_H
