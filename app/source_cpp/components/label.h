#ifndef LABEL_H
#define LABEL_H


#include "stm32f3xx_hal.h"
#include "graph.h"
#include "textout.h"
#include "control.h"



class Label: public Control
{

    uint8_t font;
    uint8_t digCount;
    TextAlign textAlign;
    char buf[20];
    char * title;
    float * fvaluep;
    int32_t * ivaluep;
    float fvalue;
    int32_t  ivalue;


public:

    Label(uint16_t x=0,uint16_t y=0,uint16_t w=0,uint16_t h=0,uint8_t fnt = 0,
           TextAlign align = Txt_Align_Center,char * name = 0);
   // void setValue(int16_t *value);
    void setValue(int32_t *value);

    void setValue(float * value);
    void setValue(int32_t  value);
    void setValue(float  value);
    void setDigCount(uint8_t  count){digCount=count;}

    void setValue(double  value){setValue((float)value);}

    //void setAction(void (*action)(void));
    void print();

};



#endif // LABEL_H
