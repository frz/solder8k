#include "vscrollbar.h"
//#include "swindow.h"

VScrollBar::VScrollBar()
{

    parentRect=0;
    minPos=0;
    maxPos=0;
    rect.x=0;
    rect.y=0;
    rect.w=0;
    rect.h=0;
    thumb.x=0;
    thumb.y=0;
    thumb.w=0;
    thumb.h=0;
    thumb.posToY=0;
    thumb.pos=0;
}

void VScrollBar::init(objRect rect,objRect *parentRect,int16_t minPos,int16_t maxPos)
{
float dpos,dl;
int tsz=0;
visible=1;
this->rect=rect;
this->parentRect=parentRect;
this->maxPos=maxPos;
this->minPos=minPos;

dl=this->rect.h;                            //real size to move
dpos=(this->maxPos - this->minPos);         //effective size to move
if (dl>=dpos) {visible=0; return;}
tsz=(int)((dl*dl)/dpos);
if (tsz<MIN_VTHUMB_SIZE) thumb.h=MIN_VTHUMB_SIZE;
        else thumb.h=tsz;
thumb.posToY=dl/dpos;
thumb.x=this->rect.x;
thumb.w=this->rect.w;
}

int16_t VScrollBar::setThumbPos(int16_t pos)
{
if ((pos<minPos)||(pos>maxPos)) return thumb.pos;
thumb.pos=pos;
thumb.y=rect.y+pos*thumb.posToY;
return thumb.pos;
}

void VScrollBar::print()
{
    if (!visible) return;
    Graph_rect(rect.x+parentRect->x,rect.y+parentRect->y,rect.w,rect.h,RS_Dotted);
    Graph_rect((int)thumb.x+parentRect->x,(int)(thumb.y+0.5)+parentRect->y,thumb.w,thumb.h,RS_White_White);
    Graph_rect((int)thumb.x+parentRect->x,(int)(thumb.y+0.5)+1+parentRect->y,thumb.w,thumb.h-2,RS_Black_Black);
}

