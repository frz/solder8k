#include "control.h"
#include "extevents.h"
Control::Control(int x, int y, int w, int h, bool visible,
                 bool selected, bool selectable, bool highlight)
{
    rect.x=x;
    rect.y=y;
    rect.w=w;
    rect.h=h;
    this->visible=visible;
    this->selected=selected;
    this->selectable=selectable;
    this->highlight=highlight;
}
bool Control::getSelected(int16_t * addrLine)
{
    if (addrLine != 0) addrLine[0]=-1;
    return selected;
}
