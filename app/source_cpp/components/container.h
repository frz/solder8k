#ifndef CONTAINER_H
#define CONTAINER_H

#include "control.h"
#include "stm32f3xx_hal.h"
#define CONTAINER_MAX_SIZE 50

//enum ElemAlign {EA_Center,EA_Left, EA_Right};

class Container : public Control
{
protected:
    Control ** elem;
    int16_t elemCount;
    int16_t activElem;
    int16_t size;


public:
    Container(uint8_t size = 15);
    void setSelected(bool selected, int16_t * addrLine = 0);
    bool getSelected(int16_t * addrLine);
    virtual bool addElem(Control *control);
    void setVisible(bool visible);
    ~Container();
};

#endif // CONTAINER_H
