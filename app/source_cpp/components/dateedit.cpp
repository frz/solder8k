#include "dateedit.h"
#include "textout.h"

void DateEdit::setButtonPos()
{
    int16_t x0,y0;
    x0=rect.x;
    y0=rect.y;

    //while (il>0)
    for(int k=0;k<3;k++)
    {
        digit[k*2].font=font;
        digit[k*2].setRect(x0,y0,digWidth,digHeight);
        x0+=digWidth;
        digit[k*2+1].font=font;
        digit[k*2+1].setRect(x0,y0,digWidth,digHeight);
        x0+=digWidth;
        dotX[k]=x0;
        x0+=dotWidth;
    }
        rect.w=x0-dotWidth-rect.x;
}

DateEdit::DateEdit(int16_t x,int16_t y,int16_t w,int16_t h,uint8_t font)
{

    digNum=0;
    activButtonNum=5;
    selectable=true;
    setRect(x,y,w,h);
    this->font=font;
    dotWidth = TextOut_getStringWidth(font,(char*)".")+1;
    digWidth = TextOut_getStringWidth(font,(char*)"0")+2;
    digHeight = TextOut_getStringHeight(font);
    if (digWidth<(rect.w-2*dotWidth)/6) digWidth = (rect.w-2*dotWidth)/6;
    if (digHeight<rect.h) digHeight = rect.h;
    rect.h=digHeight;
    //tmpdate.dd=01;
    //tmpdate.mm=01;
    //tmpdate.yy=17;
    //tmpdate=date;
    RTC_getDateTime(NULL,&tmpdate);
    loadValue2Dig(&tmpdate);
    setButtonPos();
}

void DateEdit::loadValue2Dig(RDate *date)
{
    digit[0].ivalue=date->dd/10;
    digit[1].ivalue=date->dd%10;

    digit[2].ivalue=date->mm/10;
    digit[3].ivalue=date->mm%10;

    digit[4].ivalue=date->yy/10;
    digit[5].ivalue=date->yy%10;
}

void DateEdit::loadDig2Value(RDate *date)
{
    date->dd=digit[0].ivalue*10+digit[1].ivalue;
    date->mm=digit[2].ivalue*10+digit[3].ivalue;
    date->yy=digit[4].ivalue*10+digit[5].ivalue;

    date->yy%=100;
    date->mm%=13;
    uint8_t d=1;
    if ((date->mm == 2)&&(date->yy%4 == 0)) d=2;
    date->dd%=(dim[date->mm]+d);
}

void DateEdit::setDate(uint8_t dd,uint8_t mm,uint8_t yy)
{
    tmpdate.yy=yy%100;
    tmpdate.mm=mm%13;
    uint8_t d=1;
    if ((tmpdate.mm == 2)&&((tmpdate.yy%4) == 0)) d=2;
    tmpdate.dd=dd%(dim[tmpdate.mm]+d);
}

RDate DateEdit::getDate()
{
    return tmpdate;
}

bool DateEdit::moveCursorRight()
{
   if (activButtonNum < 5)
   {
       digit[activButtonNum].setSelected(false);
       activButtonNum++;
       digit[activButtonNum].setSelected(true);
       return true;
   }
   return false;
}

bool DateEdit::moveCursorLeft()
{
    if (activButtonNum > 0)
    {
        digit[activButtonNum].setSelected(false);
        activButtonNum--;
        digit[activButtonNum].setSelected(true);
        return true;
    }
    return false;

}

bool DateEdit::addDay(int8_t dd)
{
    int8_t d=1;
    if ((tmpdate.mm == 2)&&((tmpdate.yy%4) == 0)) d=2;
    if (((tmpdate.dd+dd)<(dim[tmpdate.mm]+d))&&(tmpdate.dd+dd>0))
    {
        tmpdate.dd+=dd;
        return true;
    }
    return false;
}
bool DateEdit::addMonth(int8_t mm)
{
    int8_t d=1;
    if ((tmpdate.mm+mm < 1)||(tmpdate.mm+mm > 12)) return false;

    if ((tmpdate.mm+mm == 2)&&((tmpdate.yy%4) == 0)) d=2;
    tmpdate.mm+=mm;

    if ((dim[tmpdate.mm]+d) <= tmpdate.dd)
    {
        tmpdate.dd=dim[tmpdate.mm]+d-1;
    }
    return true;

}

bool DateEdit::addYear(int8_t yy)
{
    int8_t d=1;
    if ((tmpdate.yy+yy < 0)||(tmpdate.yy+yy > 99)) return false;
    tmpdate.yy+=yy;
    if ((tmpdate.mm == 2)&&(((tmpdate.yy)%4) == 0)) d=2;
    if ((dim[tmpdate.mm]+d) <= tmpdate.dd)
    {
        tmpdate.dd=dim[tmpdate.mm]+d-1;
    }

    return true;
}

bool DateEdit::changeValUp()
{
    switch (activButtonNum)
    {
    case 0:
        return addDay(10);
        break;
    case 1:
        return addDay(1);
        break;
    case 2:
        return addMonth(10);
        break;
    case 3:
        return addMonth(1);
        break;
    case 4:
        return addYear(10);
        break;
    case 5:
        return addYear(1);
        break;
    default:
        return false;
        break;
    }
}
bool DateEdit::changeValDown()
{
    switch (activButtonNum)
    {
    case 0:
        return addDay(-10);
        break;
    case 1:
        return addDay(-1);
        break;
    case 2:
        return addMonth(-10);
        break;
    case 3:
        return addMonth(-1);
        break;
    case 4:
        return addYear(-10);
        break;
    case 5:
        return addYear(-1);
        break;
    default:
        return false;
        break;
    }
}


bool DateEdit::keyPressed(uint32_t keyState)
{
if (activButtonNum<0) return false;
        switch (keyState)
        {

        case KeyEnter:
            loadDig2Value(&tmpdate);
            return false;
            break;
        case KeyRight:
            return moveCursorRight();
            break;
        case KeyLeft:
            return moveCursorLeft();
            break;
        case KeyUp:
            loadDig2Value(&tmpdate);
            if (changeValUp()) loadValue2Dig(&tmpdate);
            return true;
            break;
        case KeyDown:
            loadDig2Value(&tmpdate);
            if (changeValDown()) loadValue2Dig(&tmpdate);
            return true;
            break;
        default:
            return false;
            break;
        }
  return false;
}

void DateEdit::setVisible(bool visible)
{
    this->visible=true;
    for (int k=0; k<6; k++) digit[k].setVisible(visible);
}

void DateEdit::print()
{
    Graph_rect(rect.x,rect.y,rect.w,rect.h,RS_White_White);
    for(int k=0;k<6;k++)
    {
        digit[k].print();
    }
 TextOut_printStringAlign(dotX[0],rect.y,dotWidth,digHeight,
                                            TS_Black_White,Txt_Align_Left,font,(char*)".");
 TextOut_printStringAlign(dotX[1],rect.y,dotWidth,digHeight,
                                            TS_Black_White,Txt_Align_Left,font,(char*)".");
}

void DateEdit::setSelected(bool selected,int16_t *addrLine)
{
    Control::setSelected(selected);
    //if (activButtonNum>=0)
    //activButtonNum=5;
    digit[activButtonNum].setSelected(selected);
    //activButtonNum=5;
    loadValue2Dig(&tmpdate);
}
