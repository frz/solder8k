#include "timeedit.h"
#include "textout.h"

void TimeEdit::setButtonPos()
{
    int16_t x0,y0;
    x0=rect.x;
    y0=rect.y;

    //while (il>0)
    for(int k=0;k<digCount/2;k++)
    {
        digit[k*2].font=font;
        digit[k*2].setRect(x0,y0,digWidth,digHeight);
        x0+=digWidth;
        digit[k*2+1].font=font;
        digit[k*2+1].setRect(x0,y0,digWidth,digHeight);
        x0+=digWidth;
        dotX[k]=x0;
        x0+=dotWidth;
    }
    rect.w=x0-dotWidth-rect.x;
}

TimeEdit::TimeEdit(int16_t x, int16_t y, int16_t w, int16_t h, uint8_t font, bool sec)
{
    activButtonNum=3;
    digCount=4;
    secEdit=sec;
    if(secEdit)
    {
        activButtonNum=5;
     digCount= 6;
    }
    selectable=true;
    setRect(x,y,w,h);
    this->font=font;
    dotWidth = TextOut_getStringWidth(font,(char*)":")+1;
    digWidth = TextOut_getStringWidth(font,(char*)"0")+2;
    digHeight = TextOut_getStringHeight(font);

    if (digWidth<(rect.w-2*dotWidth)/digCount) digWidth = (rect.w-2*dotWidth)/digCount;
    if (digHeight<rect.h) digHeight = rect.h;
    rect.h=digHeight;

    RTC_getDateTime(&tmptime,NULL);
    if (!secEdit) tmptime.ss=0;

    loadValue2Dig(&tmptime);
    setButtonPos();
}

void TimeEdit::loadValue2Dig(RTime *time)
{
    digit[0].ivalue=time->hh/10;
    digit[1].ivalue=time->hh%10;

    digit[2].ivalue=time->mm/10;
    digit[3].ivalue=time->mm%10;

    digit[4].ivalue=time->ss/10;
    digit[5].ivalue=time->ss%10;

}

void TimeEdit::loadDig2Value(RTime *time)
{
    time->hh=digit[0].ivalue*10+digit[1].ivalue;
    time->mm=digit[2].ivalue*10+digit[3].ivalue;
    time->ss=digit[4].ivalue*10+digit[5].ivalue;
    time->hh%=24;
    time->mm%=60;
    time->ss%=60;

}

void TimeEdit::setTime(uint8_t hh,uint8_t mm,uint8_t ss)
{
    tmptime.hh=hh%24;
    tmptime.mm=mm%60;
    tmptime.ss=ss%60;
}

RTime TimeEdit::getTime()
{
    return tmptime;
}

bool TimeEdit::moveCursorRight()
{
   if (activButtonNum < digCount-1)
   {
       digit[activButtonNum].setSelected(false);
       activButtonNum++;
       digit[activButtonNum].setSelected(true);
       return true;
   }
   return false;

}

bool TimeEdit::moveCursorLeft()
{
    if (activButtonNum > 0)
    {
        digit[activButtonNum].setSelected(false);
        activButtonNum--;
        digit[activButtonNum].setSelected(true);
        return true;
    }
    return false;

}

bool TimeEdit::addHour(int8_t hh)
{
    if ((tmptime.hh+hh < 0)||(tmptime.hh+hh>23)) return false;
        tmptime.hh+=hh;
        return true;
}

bool TimeEdit::addMin(int8_t mm)
{
    if ((tmptime.mm+mm < 0)||(tmptime.mm+mm > 59)) return false;

    tmptime.mm+=mm;
    return true;

}
bool TimeEdit::addSec(int8_t ss)
{
    if ((tmptime.ss+ss < 0)||(tmptime.ss+ss > 59)) return false;

    tmptime.ss+=ss;
    return true;

}

bool TimeEdit::changeValUp()
{
    switch (activButtonNum)
    {
    case 0:
        return addHour(10);
        break;
    case 1:
        return addHour(1);
        break;
    case 2:
        return addMin(10);
        break;
    case 3:
        return addMin(1);
        break;
    case 4:
        return addSec(10);
        break;
    case 5:
        return addSec(1);
        break;

    default:
        return false;
        break;
    }
}
bool TimeEdit::changeValDown()
{
    switch (activButtonNum)
    {
    case 0:
        return addHour(-10);
        break;
    case 1:
        return addHour(-1);
        break;
    case 2:
        return addMin(-10);
        break;
    case 3:
        return addMin(-1);
        break;
    case 4:
        return addSec(-10);
        break;
    case 5:
        return addSec(-1);
        break;

    default:
        return false;
        break;
    }
}


bool TimeEdit::keyPressed(uint32_t keyState)
{
if (activButtonNum<0) return false;
        switch (keyState)
        {

        case KeyEnter:
            //loadDig2Value(&tmptime);
            return false;
            break;
        case KeyRight:
            return moveCursorRight();
            break;
        case KeyLeft:
            return moveCursorLeft();
            break;
        case KeyUp:
            //loadDig2Value(&tmptime);
            changeValUp();
            //loadDig2Value(&tmptime);
            //if (changeValUp())
            loadValue2Dig(&tmptime);
            return true;
            break;
        case KeyDown:
            changeValDown();
            //loadDig2Value(&tmptime);
            //loadDig2Value(&tmptime);
            //if (changeValDown())
            loadValue2Dig(&tmptime);
            return true;
            break;
        default:
            return false;
            break;
        }
  return false;
}

void TimeEdit::setVisible(bool visible)
{
    this->visible=true;
    for (int k=0; k<digCount; k++) digit[k].setVisible(visible);
}

void TimeEdit::print()
{
    Graph_rect(rect.x,rect.y,rect.w,rect.h,RS_White_White);
    for(int k=0;k<digCount;k++)
    {
        digit[k].print();
    }
 TextOut_printStringAlign(dotX[0],rect.y,dotWidth,digHeight,
                                            TS_Black_White,Txt_Align_Left,font,(char*)":");
}

void TimeEdit::setSelected(bool selected,int16_t *addrLine)
{
    Control::setSelected(selected);
    digit[activButtonNum].setSelected(selected);
    loadValue2Dig(&tmptime);
}

