#ifndef DATETIMELABEL_H
#define DATETIMELABEL_H

#include "stm32f3xx_hal.h"
#include "graph.h"
#include "textout.h"
#include "control.h"
#include "rtc.h"

class DateTimeLabel: public Control
{

    uint8_t font;
    RTime rTime;
    RDate rDate;
    RTime * pTime;
    RDate * pDate;
    bool fullForm;
    char buf[12];

    uint16_t digWidth;
    uint16_t dotWidth;
    uint16_t dotX[2];
    uint16_t digHeight;
    uint16_t labelLength;
public:

    DateTimeLabel(uint16_t x=0, uint16_t y=0, uint8_t fnt = SML_FONT, bool fullForm = true);
    void setDateTime(RTime time, RDate date);
    void setDateTimePtr(RTime * time, RDate * date);

    void print();

};



#endif // DATETIMELABEL_H
