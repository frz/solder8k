#ifndef TIMEEDIT_H
#define TIMEEDIT_H

#include "control.h"
#include "rtc.h"
#include "stm32f3xx_hal.h"
#include "digbutton.h"

/*struct Time
{
    int8_t hh;
    int8_t mm;
    int8_t ss;
};*/

class TimeEdit: public Control
{
    DigButton digit[6];
    int8_t activButtonNum;
    //int8_t valueDig[DIG_COUNT];
    uint8_t font;
    uint16_t digWidth;
    uint16_t dotWidth;
    uint16_t dotX[2];
    uint16_t digHeight;
    //RTime time;
    RTime tmptime;
    bool secEdit;
    uint8_t digCount;
void setButtonPos();
void value2Dig();

public:
    TimeEdit(int16_t x, int16_t y, int16_t w=0, int16_t h=0, uint8_t font=SML_FONT,bool sec = false);
    void loadValue2Dig(RTime *time);
    void loadDig2Value(RTime *time);
    RTime getTime();
    void setTime(uint8_t hh,uint8_t mm,uint8_t ss = 0);
    bool moveCursorRight();
    bool moveCursorLeft();
    bool keyPressed(uint32_t keyState);
    void setVisible(bool visible);
    void print();
    void setSelected(bool selected,int16_t *addrLine);
    bool addHour(int8_t hh);
    bool addMin(int8_t mm);
    bool addSec(int8_t ss);
    bool changeValDown();
    bool changeValUp();
};
#endif // TIMEEDIT_H
