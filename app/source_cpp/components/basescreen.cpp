#include "basescreen.h"
#include "extevents.h"
#include "graph.h"
#include "scrmanage.h"
extern ScreenManager screenManager;

bool fActionCancel(BaseScreen * parent)
{
    screenManager.closeCurrentScreen();
    screenManager.openPrevScreen();
    return true;
}

BaseScreen::BaseScreen(uint16_t x, uint16_t y, uint16_t w, uint16_t h)
{
Control::setRect(x,y,w,h);
actionEnter=NULL;
//actionCancel=NULL;
this->actionCancel=fActionCancel;
Control::setSelected(true);
}
void BaseScreen::print(void)
{
    Graph_rect(rect.x,rect.y,rect.w,rect.h,RS_White_White);
    for(int k=0;k<elemCount;k++)
    {
        elem[k]->print();
    }
}
bool BaseScreen::moveCursorUp()
{
    int tmp;
    tmp = activElem-1;
    while ((tmp>=0)&&(elem[tmp]->selectable==0)) tmp--;
    if (tmp < 0) return false;
    if (activElem>=0)
    elem[activElem]->setSelected(false);
    activElem=tmp;
    elem[activElem]->setSelected(true);
    return true;
}
bool BaseScreen::moveCursorDown()
{
    int tmp;
    tmp = activElem+1;
    while ((tmp<elemCount)&&(elem[tmp]->selectable==0)) tmp++;
    if (tmp >= elemCount) return false;
    if (activElem>=0)
        elem[activElem]->setSelected(false);
    activElem=tmp;
    elem[activElem]->setSelected(true);
    return true;
}
bool BaseScreen::keyPressed(uint32_t keyState)
{
    if (elemCount<=0) return false;
    if ((activElem<0)||(!elem[activElem]->keyPressed(keyState)))
    {
        switch (keyState)
        {
        case KeyEnter:
            if (actionEnter==NULL) return false;
            return actionEnter(this);
            break;
        case KeyCancel:
            if (actionCancel==NULL) return false;
            return actionCancel(this);
            break;
        case KeyUp:
            return moveCursorUp();
            break;
        case KeyDown:
            return moveCursorDown();
            break;
        case KeyLeft:
            return moveCursorUp();
            break;
        case KeyRight:
            return moveCursorDown();
            break;
        default:
            return false;
            break;
        }
    }
return true;
}


