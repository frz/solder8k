#include "valedit.h"

#include "textout.h"

void ValEdit::setButtonPos()
{
    int16_t x0,y0,l;
    int16_t fl=flength;
    int16_t il=ilength;
    x0=rect.x;
    y0=rect.y;
    l=length-1;

    if (isSigned)
    {
        valSign = 1;
        sign.font=font;
        sign.setSign('+');
        sign.setRect(x0,y0,digWidth,digHeight);
        x0+=digWidth;
    }

    while (il>0)
    {
        digit[l].font=font;
        digit[l].setRect(x0,y0,digWidth,digHeight);
        l--;
        x0+=digWidth;
        il--;
    }
    dotX=x0;
    x0+=dotWidth;
    while (fl>0)
    {
        digit[l].font=font;
        digit[l].setRect(x0,y0,digWidth,digHeight);
        l--;
        x0+=digWidth;
        fl--;
    }
    rect.w=x0-rect.x;

}

ValEdit::ValEdit(int16_t x,int16_t y,int16_t w,int16_t h,
                 uint8_t font,int16_t ilen,int16_t flen,bool isSigned)
{
    int16_t l=0;
    digNum=0;
    activButton=&digit[0];
    selectable=true;
    setRect(x,y,w,h);
    this->font=font;
    ilength=ilen;
    flength=flen;
    length=ilength+flength;
    this->isSigned=isSigned;

    if (length>DIG_COUNT) return;
    dotWidth = 0;
    if (flen>0) dotWidth = TextOut_getStringWidth(font,(char*)".")+1;
    digWidth = TextOut_getStringWidth(font,(char*)"0")+2;
    digHeight = TextOut_getStringHeight(font);

    if (isSigned)
    {
        l=1;
   //     sign.setSign('+');

    }
    if (digWidth<(rect.w-dotWidth)/(length+l)) digWidth = (rect.w-dotWidth)/(length+l);
    if (digHeight<rect.h) digHeight = rect.h;
    rect.h=digHeight;
    fvalue=0;
    setButtonPos();
}

void ValEdit::loadValue2Dig()
{
    int32_t tmp;
    float ftmp=fvalue;
    static float prec[6]={1,10,100,1000,10000,100000};    
    if (isSigned)
    {
        sign.setSign('+');
        if (fvalue<0)
        {
            sign.setSign('-');
            ftmp=-ftmp;
        }
    }
    tmp=(int32_t)(ftmp*prec[flength]+0.5);
    for(int k=0; k<length; k++)
    {
        digit[k].ivalue=tmp%10;
        tmp/=10;
    }

}

void ValEdit::loadDig2Value()
{
    int32_t tmp=0;
    static float prec[6]={1,10,100,1000,10000,100000};

    for(int k=length-1; k>=0; k--)
    {
        tmp*=10;
        tmp+=digit[k].ivalue;
    }
    if (isSigned)
    {
        if (sign.ivalue == '-')
            tmp=-tmp;
    }
    fvalue = (float)(tmp)/prec[flength];
}

void ValEdit::setValue(float fval)
{
    fvalue= fval;
    loadValue2Dig();
}

float ValEdit::getValue()
{
    loadDig2Value();
    return fvalue;
}

bool ValEdit::moveCursorRight()
{
    if (digNum>0)
    {
        if (activButton != NULL) activButton -> setSelected(false);
        digNum--;
        activButton = &digit[digNum];
        activButton -> setSelected(true);
       return true;
    }
 return false;

}

bool ValEdit::moveCursorLeft()
{
int16_t lng=length-1;
if (isSigned) lng=length;

    if (digNum<lng)
    {
    if (activButton != NULL) activButton -> setSelected(false);
        digNum++;

        if ((digNum==lng)&&(isSigned))
        {
                activButton = &sign;
                activButton -> setSelected(true);
                return true;
        }

        activButton = &digit[digNum];
        activButton -> setSelected(true);
        return true;
    }
    return false;
}

bool ValEdit::keyPressed(uint32_t keyState)
{
    if ((activButton==NULL)||(!activButton->keyPressed(keyState)))
    {
        switch (keyState)
        {
        case KeyEnter:
            loadDig2Value();
            return false;
            break;
        case KeyRight:
            return moveCursorRight();
            break;
        case KeyLeft:
            return moveCursorLeft();
            break;
        default:
            return false;
            break;
        }
    }
  return true;
}
void ValEdit::setVisible(bool visible)
{
    this->visible=true;
    for (int k=0; k<length; k++) digit[k].setVisible(visible);
    sign.visible=false;
    if (isSigned) sign.visible=visible;
}

void ValEdit::print()
{
    Graph_rect(rect.x,rect.y,rect.w,rect.h,RS_White_White);
    for(int k=0;k<length;k++)
    {
        digit[k].print();
    }
    if (isSigned) sign.print();
    if (flength>0) TextOut_printStringAlign(dotX,rect.y,dotWidth,digHeight,
                                            TS_Black_White,Txt_Align_Left,font,(char*)".");
}
void ValEdit::setSelected(bool selected,int16_t *addrLine)
{
    Control::setSelected(selected);
    if (activButton!=NULL) activButton->setSelected(selected);
    activButton=&digit[0];
    digNum=0;
    loadValue2Dig();
}
