#include "label.h"
//#include "math.h"
Label::Label(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint8_t fnt, TextAlign align, char *name)
{
    rect.x=x;
    rect.y=y;
    rect.w=w;
    rect.h=h;
    title=name;
    font=fnt;
    textAlign=align;
    visible=true;
    ivaluep=NULL;
    fvaluep=NULL;
    this->selectable = false;
    digCount=2;
    buf[0]=0;
}
void Label::setValue(float *value)
{
    fvaluep = value;
    ivaluep = NULL;
}
void Label::setValue(int32_t *value)
{
    ivaluep = value;
    fvaluep = NULL;
}
void Label::setValue(float value)
{
    fvalue = value;
    fvaluep = &fvalue;
    ivaluep = NULL;

}
void Label::setValue(int32_t value)
{
    ivalue = value;
    ivaluep=&ivalue;
    fvaluep=NULL;
}


void Label::print()
{
    int tx=1,ty=0;
    if (!visible) return;
    if (fvaluep!=NULL)
    {
    float2str(*fvaluep,buf,digCount,4,0);
    title=buf;
    } else
    if (ivaluep!=NULL)
    {
    int2str(*ivaluep,buf,10);
    title=buf;
    }
    //if ()

    if (highlight)
    {
        Graph_rect(rect.x,rect.y,rect.w,rect.h,RS_Black_Black);
    //    TextOut_printStringAlign(rect.x+tx,rect.y+ty,rect.w,rect.h,TS_White_Transparent,textAlign,font,title);
    }
    else
    {
        Graph_rect(rect.x,rect.y,rect.w,rect.h,RS_White_White);
    //    TextOut_printStringAlign(rect.x+tx,rect.y+ty,rect.w,rect.h,TS_Black_Transparent,textAlign,font,title);

    }
    TextOut_printStringAlign(rect.x+tx,rect.y+ty,rect.w,rect.h,TS_XOR,textAlign,font,title);

}
