#include "table.h"
#include "extevents.h"

Table::Table(int16_t x, int16_t y, int16_t w, int16_t h, int16_t maxElemCount,
             ElemAlign align, bool scroll):Container(maxElemCount)
{
    if (scroll) w-=SCROLL_W;
    setRect(x,y,w,h);
//    setTableSize(hSize,vSize,hStep,vStep);
    this->align = align;
    this->scroll=scroll;
    vscroll.visible=0;
    firstVisibleRow = 0;
    visibleRowCount = 0;
    selectable=true;
    init=0;
}


void Table::clear()
{
    tableVSize=0;
    tableHSize=0;
    vStep = 0;
    hStep = 0;
    vscroll.visible=0;
    init=0;
    scroll=false;
}

void Table::setTableSize(int16_t hSize, int16_t vSize,int16_t hStep, int16_t vStep)
{
    if ((hSize<=0)||(vSize<=0)) return;
    tableVSize=vSize;
    this->vStep = vStep;
    if (vStep==0)
    this->vStep = rect.h/vSize;

    tableHSize=hSize;
    this->hStep = hStep;
    if (hStep==0)
    this->hStep = rect.w/hSize;
    visibleRowCount = rect.h/this->vStep;

}

bool Table::addElem(Control * control)
{
    Control ctrl;
    int y0,x0,xa,ya;
    int col,row;
    x0=rect.x;
    y0=rect.y;

    if ((tableHSize <=0 )||(tableVSize <= 0)) return false;

    col = elemCount / tableVSize;
    row = elemCount % tableVSize;
    if (col>=tableHSize) return false;
    xa=(hStep-control->rect.w)/2;
    ya=(vStep-control->rect.h)/2;

    control->rect.y=row*vStep+y0+ya;
    control->rect.x=col*hStep+x0+xa;
    if ((row >= firstVisibleRow)&&(row < firstVisibleRow+visibleRowCount)) control->visible=true;
    return Container::addElem(control);
}
void Table::setControlPos()
{
    int row,y0;
    row = activElem % tableVSize;

    if ((row >= firstVisibleRow)&&(row < firstVisibleRow+visibleRowCount)) return;

    if (row < firstVisibleRow) firstVisibleRow=row;
    if (row >= firstVisibleRow+visibleRowCount) firstVisibleRow=row-visibleRowCount+1;
    for (int k=0;k<elemCount; k++)
    {
        row = k % tableVSize;
        y0=(vStep-elem[activElem]->rect.h)/2;
        elem[k]->rect.y=(row-firstVisibleRow)*vStep+y0+rect.y;
        elem[k]->visible=false;
        if ((row >= firstVisibleRow)&&(row < firstVisibleRow+visibleRowCount)) elem[k]->visible=true;
    }
}

bool Table::moveCursorUp()
{int tmp;
    if ((activElem % tableVSize)==0) return false;

    tmp = activElem-1;

    while ((tmp>=0)&&(elem[tmp]->selectable==0)) tmp--;
    if (tmp < 0) return false;
    if (activElem>=0)
        elem[activElem]->setSelected(false);
    activElem=tmp;
    elem[activElem]->setSelected(true);
    setControlPos();
    return true;
}
bool Table::moveCursorDown()
{int tmp;
    if ((activElem % tableVSize) == (tableVSize-1)) return false;
    tmp = activElem+1;
    while ((tmp<elemCount)&&(elem[tmp]->selectable==0)) tmp++;
    if (tmp >= elemCount) return false;
    if (activElem>=0)
        elem[activElem]->setSelected(false);
    activElem=tmp;
    elem[activElem]->setSelected(true);

    setControlPos();
    return true;
}
bool Table::moveCursorLeft()
{int tmp;
    tmp = activElem - tableVSize;
    while ((tmp>=0)&&(elem[tmp]->selectable==0)) tmp-= tableVSize;
    if (tmp < 0) return true;
    if (activElem>=0)
        elem[activElem]->setSelected(false);
    activElem=tmp;
    elem[activElem]->setSelected(true);
    return true;
}
bool Table::moveCursorRight()
{int tmp;
    tmp = activElem + tableVSize;
    while ((tmp<elemCount)&&(elem[tmp]->selectable==0)) tmp+= tableVSize;
    if (tmp >= elemCount) return true;
    if (activElem>=0)
        elem[activElem]->setSelected(false);
    activElem=tmp;
    elem[activElem]->setSelected(true);
    return true;
}

bool Table::keyPressed(uint32_t keyState)
{
    if ((activElem<0)||(!elem[activElem]->keyPressed(keyState)))
    {
        switch (keyState)
        {
        case KeyUp:
            return moveCursorUp();
            break;
        case KeyDown:
            return moveCursorDown();
            break;
        case KeyRight:
            return moveCursorRight();
            break;
        case KeyLeft:
            return moveCursorLeft();
            break;
        default:
            return false;
            break;
        }
    }
return true;
}

void Table::print()
{
    Graph_rect(rect.x,rect.y,rect.w,rect.h,RS_White_White);
    for(int k=0;k<elemCount;k++)
    {
        elem[k]->print();
    }
}
void Table::setSelected(bool selected,int16_t *addrLine)
{
    Container::setSelected(selected,addrLine);
    if (selected) setControlPos();
}


