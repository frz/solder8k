#include "container.h"

Container::Container(uint8_t size)
{
    this->size=0;
    if (size<CONTAINER_MAX_SIZE)
    this->size=size;
    elem = new Control * [this->size];
    elemCount=0;
    activElem=-1;
    selectable=true;
}
Container::~Container()
{
    for(int k=0;k<elemCount;k++)
        delete elem[k];
    delete elem;
}
bool Container::addElem(Control * control)
{
    if (elemCount>=size) return false;

    elem[elemCount]=control;
    elemCount++;
    return true;
}

void Container::setSelected(bool selected, int16_t * addrLine)
{
if (selected)
{
int k;
Control::setSelected(true);
    if ((addrLine!=NULL)&&(addrLine[0]<elemCount)&&(addrLine[0]>=0))
    {
       if (addrLine[0]>=0)
       {
           activElem=addrLine[0];
           elem[addrLine[0]]->setSelected(true,&addrLine[1]);
       }
    }
    else
    {
        for( k=0;((k<elemCount)&&(!elem[k]->selectable));k++);
        if (k<elemCount)
            {
            elem[k]->setSelected(true);
            activElem=k;
            }
    }

}
else
{
    activElem=-1;
    for(int k=0;k<elemCount;k++)
        {
            elem[k]->setSelected(false);
        }
    Control::setSelected(false);
}

}

bool Container::getSelected(int16_t * addrLine)
{
   if (isSelected())
   {
       if (addrLine!=NULL) addrLine[0]=activElem;
       return elem[activElem]->getSelected(&addrLine[1]);
   }
   if (addrLine!=NULL) addrLine[0]=-1;
   return false;
}
void Container::setVisible(bool visible)
{
    for(int k=0;k<elemCount;k++)
        {
            elem[k]->setVisible(visible);
        }
    Control::setVisible(visible);
}
//Control::setSelected(selected);
/*for(int k=0;k<elemCount;k++)
    {
        elem[k]->print();
    }*/

