#ifndef DATEEDIT_H
#define DATEEDIT_H

#include "control.h"

#include "rtc.h"
#include "stm32f3xx_hal.h"
#include "digbutton.h"

//#define DT_DIG_COUNT 8
/*struct Date
{
    int8_t yy;
    int8_t mm;
    int8_t dd;
};*/

class DateEdit: public Control
{
    DigButton digit[6];
    int8_t activButtonNum;
    //int8_t valueDig[DIG_COUNT];
    uint8_t font;
    uint16_t digWidth;
    uint16_t dotWidth;
    uint16_t dotX[3];
    uint16_t digHeight;
    uint16_t digNum;
    //RDate date;
    RDate tmpdate;
    const uint8_t dim[13]={0,31,28,31,30,31,30,31,31,30,31,30,31};
    /*uint8_t yy;
    uint8_t mm;
    uint8_t dd;
    */
//    int32_t ivalue;
void setButtonPos();
void value2Dig();

public:
    //float   fvalue;
    DateEdit(int16_t x, int16_t y, int16_t w, int16_t h, uint8_t font);
    void loadValue2Dig(RDate *date);
    void loadDig2Value(RDate *date);
    RDate getDate();
    void setDate(uint8_t dd,uint8_t mm,uint8_t yy);
    bool moveCursorRight();
    bool moveCursorLeft();
    bool keyPressed(uint32_t keyState);
    void setVisible(bool visible);
    void print();
    void setSelected(bool selected,int16_t *addrLine);
    bool addDay(int8_t dd);
    bool addMonth(int8_t mm);
    bool addYear(int8_t yy);
    bool changeValDown();
    bool changeValUp();

};

#endif // DATETIMECONTROL_H
