#include "digbutton.h"

DigButton::DigButton(void)
{
    min=0;
    max=9;
    ivalue=min;
    font=0;
    visible=false;
    this->selectable = true;
    this->setSelected(false);
    sign=false;
}

/*digButton::digButton(uint16_t x,uint16_t y,uint16_t w,uint16_t h,uint8_t fnt,uint8_t max,uint8_t min)
{
    rect.x=x;
    rect.y=y;
    rect.w=w;
    rect.h=h;
    ivalue=min;
    font=fnt;
    visible=false;
    this->selectable = true;
    this->setSelected(false);
}*/
void DigButton::print()
{
    char c[2]={0,0};
    int tx=0,ty=0;
    if (!visible) return;
    if ((ivalue>=min)&&(ivalue<=max))
    {
        c[0]=ivalue+'0';
    };
    if (sign) c[0]=ivalue;
    if ((highlight)||(isSelected()))
    {
        Graph_rect(rect.x,rect.y,rect.w,rect.h,RS_Black_Black);
        TextOut_printStringAlign(rect.x+tx,rect.y+ty,rect.w,rect.h,TS_White_Transparent,Txt_Align_Center,font,c);
    }
    else
    {
        Graph_rect(rect.x,rect.y,rect.w,rect.h,RS_White_White);
        TextOut_printStringAlign(rect.x+tx,rect.y+ty,rect.w,rect.h,TS_Black_Transparent,Txt_Align_Center,font,c);

    }
}
bool DigButton::keyPressed(uint32_t keyState)
{

    switch (keyState)
    {
    case KeyUp:
        ivalue++;
        if (sign) ivalue='+';
        if (ivalue>max) ivalue=max;

        return true;
        break;
    case KeyDown:
        ivalue--;
        if (sign) ivalue='-';
        if (ivalue<min) ivalue=min;

        return true;
        break;

    default:
        return false;
        break;

    }
}
