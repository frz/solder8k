#ifndef DIGBUTTON_H
#define DIGBUTTON_H
#include "stm32f3xx_hal.h"
#include "graph.h"
#include "textout.h"
#include "control.h"



class DigButton: public Control
{

public:
    uint8_t font;
    int32_t ivalue;
    int32_t max;
    int32_t min;
    bool sign;

    DigButton(void);
    void setFont(uint8_t fnt){font=fnt;}
    void setMaxMin(uint16_t max,uint16_t min)
    {
        this->max=max;
        this->min=min;
    }
    void setSign(char val)
        {
        ivalue=val;
        sign=true;
        max='-';
        min='+';
        }
    void print();
    bool keyPressed(uint32_t keyState);

};



#endif // VALBUTTON_H
