#ifndef BUTTON_H
#define BUTTON_H

#include "stm32f3xx_hal.h"
#include "graph.h"
#include "textout.h"
#include "table.h"
#include "control.h"
#include "basescreen.h"


class Button: public Control
{

    uint8_t font;
    TextAlign textAlign;
    char * title;
bool (*actionEnter)(BaseScreen *);
public:
    BaseScreen * parentScreen;
    Button(void);
    Button(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint8_t fnt = 0,
           TextAlign align = Txt_Align_Center, char * name = 0);

    void setAction(bool (*actionEnter)(BaseScreen *),BaseScreen* parent);

    void print();
    bool keyPressed(uint32_t keyState);

};

#endif // BUTTON_H
