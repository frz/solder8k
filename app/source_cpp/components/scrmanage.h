#ifndef SCRHISTORY_H
#define SCRHISTORY_H
#include "basescreen.h"
#define HISTORY_MAX_SIZE 10
#define ADDR_SIZE 5

struct HistElem
{
    BaseScreen * (*openFuncPtr)(int16_t * activElAddr);
    int16_t activElemAddr[ADDR_SIZE];
};

class ScreenManager
{
    int16_t historySize;
    HistElem history[HISTORY_MAX_SIZE];
public:
    BaseScreen * currentScreen;
    ScreenManager();
    void closeCurrentScreen(bool saveHistory = false);
    void openPrevScreen();

};

#endif // SCRHISTORY_H
