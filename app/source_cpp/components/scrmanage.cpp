#include "scrmanage.h"

ScreenManager::ScreenManager()
{
    currentScreen=NULL;
    historySize=0;
}
void ScreenManager::closeCurrentScreen(bool saveHistory)
{
    if (currentScreen!=NULL)
    {
        if ((saveHistory)&&(historySize<HISTORY_MAX_SIZE))
        {
            history[historySize].openFuncPtr=currentScreen->openFuncPtr;
            currentScreen->getSelected(history[historySize].activElemAddr);
            historySize++;
        }
        delete currentScreen;
        currentScreen=NULL;
    }
}
void  ScreenManager::openPrevScreen()
{
    if (historySize>0)
    {
        historySize--;
        currentScreen=history[historySize].openFuncPtr(history[historySize].activElemAddr);
    }
}
