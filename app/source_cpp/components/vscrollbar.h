#ifndef VSCROLLBAR_H
#define VSCROLLBAR_H

#include "graph.h"
#include "control.h"

#define MIN_VTHUMB_SIZE 5
#define VSCROLL_SZ 5

struct VThumb
{
    float x;
    float y;
    int16_t h;
    int16_t w;
    int16_t pos;
    float posToY;
};

class VScrollBar: Control
{
public:
    //objRect rect;
    objRect *parentRect;
    VThumb thumb;
    int16_t minPos;
    int16_t maxPos;
    uint8_t visible;

    VScrollBar();
    void init(objRect rect,objRect *parentRect,int16_t minPos,int16_t maxPos);
    int16_t setThumbPos(int16_t pos);
    void print();
};

#endif // VSCROLLBAR_H
