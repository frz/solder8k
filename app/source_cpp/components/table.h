#ifndef TABLE_H
#define TABLE_H

#define SCROLL_W 10

#include "container.h"
#include "vscrollbar.h"
#include "stm32f3xx_hal.h"

enum ElemAlign {EA_Center,EA_Left, EA_Right};

class Table : public Container
{

    ElemAlign align;
    int16_t tableVSize;
    int16_t tableHSize;
    //int16_t firstVisibleNum;

    int16_t vStep;
    int16_t hStep;

    int16_t firstVisibleRow;
    int16_t visibleRowCount;
   /* int16_t vNum;
    int16_t hNum;*/
    bool init;
    bool scroll;
    VScrollBar vscroll;
public:
    Table(int16_t x, int16_t y, int16_t w, int16_t h, int16_t maxElemCount=10, ElemAlign align=EA_Center, bool scroll = false);

    //void setRect(int16_t x, int16_t y, int16_t w, int16_t h);
    void setTableSize(int16_t hSize, int16_t vSize, int16_t hStep=0, int16_t vStep=0);
    void clear();
    bool addElem(Control *ctrl);

    bool moveCursorUp();
    bool moveCursorDown();
    bool moveCursorLeft();
    bool moveCursorRight();
    void setControlPos();
    void setSelected(bool selected,int16_t *addrLine = 0);

    bool keyPressed(uint32_t keyState);
    void print();
 //   void setElemPos();
};

#endif //















