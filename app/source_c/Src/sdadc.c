/**
  ******************************************************************************
  * File Name          : SDADC.c
  * Description        : This file provides code for the configuration
  *                      of the SDADC instances.
  ******************************************************************************
  *
  * COPYRIGHT(c) 2017 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "sdadc.h"

#include "gpio.h"

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

SDADC_HandleTypeDef hsdadc1;
SDADC_HandleTypeDef hsdadc2;
int8_t sdadc1Ready=0;
int16_t adcVal=0;

int32_t adc1Data[SDADC1_CHANNAL_COUNT];
AdcDataFilt sdadc1DataFilt[SDADC1_CHANNAL_COUNT];
float kU=1257.0/3040.0;

float SDADC_getV2()
{
    return (adc1Data[2]*kU)/1000.0;
}
void SDADC_proc()
{
    hsdadc1.State=HAL_SDADC_STATE_READY;

    HAL_SDADC_InjectedStart(&hsdadc1);
}

void SDADC1_IRQHandler(void )
{
            HAL_GPIO_WritePin(GPIOC,GPIO_PIN_1,GPIO_PIN_RESET);
    static int16_t t1,t2;
    CLEAR_BIT(hsdadc1.Instance->ISR,SDADC_FLAG_JEOC);
    hsdadc1.Instance->CLRISR |= SDADC_ISR_CLRJOVRF;
    t1 = HAL_SDADC_InjectedGetValue(&hsdadc1,&t2);
    sdadc1DataFilt[t2].summ+=t1;
    sdadc1DataFilt[t2].summ-=sdadc1DataFilt[t2].dat[sdadc1DataFilt[t2].last];
    sdadc1DataFilt[t2].dat[sdadc1DataFilt[t2].last]=t1;
    sdadc1DataFilt[t2].last++;
    sdadc1DataFilt[t2].last%=FILT_BUF;
    adc1Data[t2]=((sdadc1DataFilt[t2].summ>>FILT_LEVEL)+32768);
            HAL_GPIO_WritePin(GPIOC,GPIO_PIN_1,GPIO_PIN_SET);
}

//========================   INITIALIZATION ADARC 1 & 2 ========================
/* SDADC1 init function */
void MX_SDADC1_Init(void)
{
    __SDADC1_CLK_ENABLE();
    __SDADC2_CLK_ENABLE();
    __HAL_RCC_SDADC_CONFIG(RCC_SDADCSYSCLK_DIV4);

    HAL_PWREx_EnableSDADC(PWR_SDADC_ANALOG1);
    HAL_PWREx_EnableSDADC(PWR_SDADC_ANALOG2);

    /**Configure the SDADC low power mode, fast conversion mode,
    slow clock mode and SDADC1 reference voltage 
    */
  hsdadc1.Instance = SDADC1;
  hsdadc1.Init.IdleLowPowerMode = SDADC_LOWPOWER_NONE;
  hsdadc1.Init.FastConversionMode = SDADC_FAST_CONV_DISABLE;
  hsdadc1.Init.SlowClockMode = SDADC_SLOW_CLOCK_DISABLE;
  hsdadc1.Init.ReferenceVoltage = SDADC_VREF_EXT;
  if (HAL_SDADC_Init(&hsdadc1) != HAL_OK)
  {
    Error_Handler();
  }

SDADC1_Init_Channels();
}
/* SDADC2 init function */
void MX_SDADC2_Init(void)
{

    /**Configure the SDADC low power mode, fast conversion mode,
    slow clock mode and SDADC1 reference voltage 
    */
  hsdadc2.Instance = SDADC2;
  hsdadc2.Init.IdleLowPowerMode = SDADC_LOWPOWER_NONE;
  hsdadc2.Init.FastConversionMode = SDADC_FAST_CONV_DISABLE;
  hsdadc2.Init.SlowClockMode = SDADC_SLOW_CLOCK_DISABLE;
  hsdadc2.Init.ReferenceVoltage = SDADC_VREF_EXT;
  if (HAL_SDADC_Init(&hsdadc2) != HAL_OK)
  {
    Error_Handler();
  }

}

void HAL_SDADC_MspInit(SDADC_HandleTypeDef* sdadcHandle)
{

  GPIO_InitTypeDef GPIO_InitStruct;
  if(sdadcHandle->Instance==SDADC1)
  {
  /* USER CODE BEGIN SDADC1_MspInit 0 */

  /* USER CODE END SDADC1_MspInit 0 */
    /* Peripheral clock enable */
    __HAL_RCC_SDADC1_CLK_ENABLE();
  
    /**SDADC1 GPIO Configuration    
    PB0     ------> SDADC1_AIN6P
    PB1     ------> SDADC1_AIN5P
    PB2     ------> SDADC1_AIN4P
    PE7     ------> SDADC1_AIN3P
    PE8     ------> SDADC1_AIN8P
    PE9     ------> SDADC1_AIN7P
    PE10     ------> SDADC1_AIN2P
    PE11     ------> SDADC1_AIN1P 
    */
    GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2;
    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_10
                          |GPIO_PIN_11;
    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);
  }
  else if(sdadcHandle->Instance==SDADC2)
  {
    __HAL_RCC_SDADC2_CLK_ENABLE();
  
    /**SDADC2 GPIO Configuration    
    PE12     ------> SDADC2_AIN3P
    PE13     ------> SDADC2_AIN2P
    PE14     ------> SDADC2_AIN1P
    PE15     ------> SDADC2_AIN0P 
    */
    GPIO_InitStruct.Pin = GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15;
    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  }
}


void SDADC1_Init_Channels()
{
    SDADC_ConfParamTypeDef confParam;
    confParam.CommonMode=SDADC_COMMON_MODE_VSSA;
    confParam.Gain=SDADC_GAIN_1;
    confParam.InputMode=SDADC_INPUT_MODE_SE_ZERO_REFERENCE;
   // confParam.InputMode=SDADC_INPUT_MODE_SE_OFFSET;
    confParam.Offset=0;
    HAL_SDADC_PrepareChannelConfig(&hsdadc1,SDADC_CONF_INDEX_0,&confParam);

    HAL_SDADC_AssociateChannelConfig(&hsdadc1,
                                     SDADC_CHANNEL_1|
                                     SDADC_CHANNEL_2|

                                     SDADC_CHANNEL_4|
                                     SDADC_CHANNEL_5|
                                     SDADC_CHANNEL_6,
                                     SDADC_CONF_INDEX_0);
    HAL_SDADC_SelectInjectedDelay(&hsdadc1,SDADC_INJECTED_DELAY_NONE);
    HAL_SDADC_SelectInjectedTrigger(&hsdadc1,SDADC_SOFTWARE_TRIGGER);
    HAL_SDADC_InjectedConfigChannel(&hsdadc1,
                                    SDADC_CHANNEL_1|
                                    SDADC_CHANNEL_2|

                                    SDADC_CHANNEL_4|
                                    SDADC_CHANNEL_5|
                                    SDADC_CHANNEL_6,
                                    SDADC_CONTINUOUS_CONV_OFF);
    HAL_NVIC_SetPriority(SDADC1_IRQn,1,2);
    HAL_NVIC_EnableIRQ(SDADC1_IRQn);

    //hsdadc1.

    HAL_SDADC_CalibrationStart(&hsdadc1,SDADC_CALIBRATION_SEQ_1);
        while (!(hsdadc1.Instance->ISR & SDADC_FLAG_EOCAL)){};
        hsdadc1.State=HAL_SDADC_STATE_READY;
      HAL_SDADC_InjectedStart_IT(&hsdadc1);
}

void HAL_SDADC_MspDeInit(SDADC_HandleTypeDef* sdadcHandle)
{

  if(sdadcHandle->Instance==SDADC1)
  {
    __HAL_RCC_SDADC1_CLK_DISABLE();
    HAL_GPIO_DeInit(GPIOB, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2);

    HAL_GPIO_DeInit(GPIOE, GPIO_PIN_10
                          |GPIO_PIN_11);
  }
  else if(sdadcHandle->Instance==SDADC2)
  {
    __HAL_RCC_SDADC2_CLK_DISABLE();
    HAL_GPIO_DeInit(GPIOE, GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15);
  }
} 






/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
