#ifndef FONTS_H
#define FONTS_H
#ifdef __cplusplus
 extern "C" {
#endif
 #include "stm32f3xx_hal.h"

#define MAX_FONT_WIDTH 20
#define CHAR_COUNT 156
#define FONTS_COUNT 3

#define SML_FONT 0
#define MED_FONT 1
#define BIG_FONT 2



typedef struct
{
unsigned char sym;
uint8_t width;
uint32_t bitmask[MAX_FONT_WIDTH];
} GChar;

typedef struct
{
uint8_t height;
GChar gChars[CHAR_COUNT];
} Font;

 typedef struct
 {
 const uint8_t width;
 const uint8_t bitmask[];
 } BitChar;

 typedef struct
 {
     uint8_t height;
     uint8_t maskVSize;
     const BitChar * bitChar[256];
 } BitFont;



#ifdef __cplusplus
}
#endif
#endif
