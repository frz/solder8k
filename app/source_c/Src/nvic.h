//#define USE_STDPERIPH_DRIVER
#ifndef NVIC_H
#define NVIC_H
#ifdef __cplusplus
 extern "C" {
#endif

#include "stm32f3xx_hal.h"

 void NVIC_TIM4_interrupt_enable();
void NVIC_DMA2_interrupt_enable();
void NVIC_TIM12_interrupt_enable();
#ifdef __cplusplus
 }
#endif
#endif
