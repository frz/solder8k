#include "nvic.h"


void NVIC_TIM4_interrupt_enable()
{
 HAL_NVIC_SetPriority(TIM4_IRQn,1,0);
 HAL_NVIC_EnableIRQ(TIM4_IRQn);
}

void NVIC_TIM12_interrupt_enable()
{
 HAL_NVIC_SetPriority(TIM12_IRQn,2,1);
 HAL_NVIC_EnableIRQ(TIM12_IRQn);
}

void NVIC_DMA2_interrupt_enable()
{
    HAL_NVIC_SetPriority(DMA2_Channel1_IRQn,1,1);
    HAL_NVIC_EnableIRQ(DMA2_Channel1_IRQn);

    HAL_NVIC_SetPriority(DMA2_Channel2_IRQn,1,2);
    HAL_NVIC_EnableIRQ(DMA2_Channel2_IRQn);
}
/*vvoid NVIC_buttons_interrupt_enable()
{
	NVIC_init_str.NVIC_IRQChannel=EXTI9_5_IRQn;
	NVIC_init_str.NVIC_IRQChannelPreemptionPriority=0;
	NVIC_init_str.NVIC_IRQChannelSubPriority=0;
	NVIC_init_str.NVIC_IRQChannelCmd=ENABLE;
	NVIC_Init(&NVIC_init_str);
}

*/
