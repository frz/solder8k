#ifndef RTC_H
#define RTC_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f3xx_hal.h"
typedef struct
{
    uint8_t hh;
    uint8_t mm;
    uint8_t ss;
} RTime;
typedef struct
{
    uint8_t dd;
    uint8_t mm;
    uint8_t yy;
} RDate;
void RTC_init();

void RTC_getDateTime(RTime * rTime,RDate * rDate);//������� �����
void HAL_RTC_MspInit(RTC_HandleTypeDef* hrtc);
void RTC_setDateTime(RTime *rTime, RDate *rDate);
void RTC_proc();

#ifdef __cplusplus
}
#endif
#endif
