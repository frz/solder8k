#include  "timers.h"
#include  "nvic.h"
#include  "sdadc.h"
#include  "rtc.h"
#include  "extevents.h"

TIM_HandleTypeDef tim4Handle;
TIM_HandleTypeDef tim12Handle;
uint32_t timerPWM=0;
//#include "stm32f3xx_hal_tim.h"

//��������� ��� sys clk=16MHz

void TIMERS_init()
{
    //TIM_HandleTypeDef timHandle;
    tim4Handle.Instance=TIM4;
    tim4Handle.Init.AutoReloadPreload=TIM_AUTORELOAD_PRELOAD_DISABLE;
    //tim4Handle.Init.ClockDivision=TIM_CLOCKDIVISION_DIV2;
    tim4Handle.Init.CounterMode=TIM_COUNTERMODE_UP;
    tim4Handle.Init.Period=5000;
    tim4Handle.Init.Prescaler=3;// devider=2 !!!! freq= 16MHz/5K/2=1,6K

     HAL_TIM_Base_MspInit(&tim4Handle);
     //__TIM4_CLK_ENABLE;
     __HAL_RCC_TIM4_CLK_ENABLE();

     HAL_TIM_Base_Init(&tim4Handle);
     NVIC_TIM4_interrupt_enable();
     HAL_TIM_Base_Start_IT(&tim4Handle);


     __HAL_RCC_TIM12_FORCE_RESET();
     __HAL_RCC_TIM12_RELEASE_RESET();

      __HAL_RCC_TIM12_CLK_ENABLE();
     // __HAL_RCC_//(RCC_APB2Periph_AFIO
        GPIO_InitTypeDef GPIO_InitStruct;
      GPIO_InitStruct.Pin = GPIO_PIN_14;
      GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
      GPIO_InitStruct.Alternate=GPIO_AF9_TIM12;
      GPIO_InitStruct.Pull = GPIO_NOPULL;
      GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
      HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

     tim12Handle.Instance=TIM12;
     tim12Handle.Init.AutoReloadPreload=TIM_AUTORELOAD_PRELOAD_DISABLE;
     //tim4Handle.Init.ClockDivision=TIM_CLOCKDIVISION_DIV2;
     tim12Handle.Init.CounterMode=TIM_COUNTERMODE_UP;

     tim12Handle.Init.Period=10000;
     tim12Handle.Init.Prescaler=799;//freq= 16MHz/800=20�/10000=2��
     //HAL_TIM_Base_Init(&tim12Handle);
     HAL_TIM_PWM_MspInit(&tim12Handle);
     HAL_TIM_PWM_Init(&tim12Handle);// and HAL_TIM_PWM_ConfigChannel:
     TIM_OC_InitTypeDef oc;
     oc.OCFastMode=TIM_OCFAST_ENABLE;
     oc.OCMode=TIM_OCMODE_PWM1;
     oc.OCPolarity=TIM_OCPOLARITY_LOW;
     oc.Pulse=2000;
     HAL_TIM_PWM_ConfigChannel(&tim12Handle,&oc,TIM_CHANNEL_1);
     HAL_TIM_PWM_Start(&tim12Handle,TIM_CHANNEL_1);
     __HAL_TIM_ENABLE_IT(&tim12Handle, TIM_IT_UPDATE);
    NVIC_TIM12_interrupt_enable();
     //__HAL_TIM_ENABLE(&tim12Handle);

}



void TIM4_IRQHandler(void )
{
    __HAL_TIM_CLEAR_IT(&tim4Handle, TIM_IT_UPDATE);

    static uint8_t s=0;
    SDADC_proc();
    s++;
    ExtEvents_keyProc();
    if (s == 4)
    {
       RTC_proc();
        s=0;
    }


}

void TIM12_IRQHandler(void )
{
__HAL_TIM_CLEAR_IT(&tim12Handle, TIM_IT_UPDATE);
timerPWM++;

}

