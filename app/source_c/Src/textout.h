#ifndef TEXTOUT_H
#define TEXTOUT_H
#ifdef __cplusplus
 extern "C" {
#endif
 #include "stm32f3xx_hal.h"
#include "lcd128x64.h"
#include "fonts.h"

typedef enum
{
    TS_XOR,
    TS_Black_White,
    TS_White_Black,
    TS_Black_Transparent,
    TS_White_Transparent
} TextStyle;

typedef enum
{
    Txt_Align_Left,
    Txt_Align_Center,
    Txt_Align_Right
} TextAlign;





uint8_t TextOut_printChar(uint8_t x,uint8_t y,TextStyle style,uint8_t fnt,unsigned char c);
uint16_t TextOut_printString(uint8_t x,uint8_t y,TextStyle style,uint8_t fnt, char* str);
uint16_t TextOut_printStringAlign(int16_t x,int16_t y,int16_t w,int16_t h,TextStyle style,TextAlign align,uint8_t fnt, char* str);

uint16_t TextOut_getStringWidth(uint8_t fnt, char* str);
uint16_t TextOut_getStringHeight(uint8_t fnt);
int16_t float2str(float f,char* buf,int16_t sz,uint8_t prec, uint8_t zeros);
int16_t int2str(int i, char* buf, int16_t sz);
#ifdef __cplusplus
 }
#endif
#endif
