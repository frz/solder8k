#ifndef GRAPH_H
#define GRAPH_H
#ifdef __cplusplus
 extern "C" {
#endif
 #include "stm32f3xx_hal.h"

typedef enum
{
	RS_Black_Black,
	RS_Black_White,
	RS_White_White,
	RS_Black_Transparent,
	RS_White_Transparent,
	RS_Dotted,
} Rect_Style;

typedef enum
{
	LS_Black,
	LS_White,
	LS_Dotted0,
	LS_Dotted1
} Line_Style;

void Graph_rect(uint8_t x,uint8_t y,uint8_t w,uint8_t h,Rect_Style style);

void Graph_Vline(uint8_t x,uint8_t y,uint8_t len,Line_Style);
void Graph_Hline(uint8_t x,uint8_t y,uint8_t len,Line_Style);

#ifdef __cplusplus
}
#endif
#endif
