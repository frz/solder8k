
#include "textout.h"
#include "bitfont1.h"
#include "bitfontbigdig.h"
#include "bitfontmeddig.h"

extern const BitFont bitFont2;
extern const BitFont bitFont1;
extern const BitFont bitFont0;

const BitFont *bfonts[3]={&bitFont0,&bitFont1,&bitFont2};


uint8_t TextOut_printChar(uint8_t x,uint8_t y,TextStyle style,uint8_t fnt,unsigned char c)
{
int k=0;
uint32_t ln=0;
uint16_t sp=0,vs=0;

if (bfonts[fnt]->bitChar[c]==0) return 0;
//if (c<123) c-=32; else c-=101;
//l=m[1]->gchar[1];
//c%=155;
switch (style)
{
case TS_Black_White:

        for (k=0;k<bfonts[fnt]->bitChar[c]->width;k++)
        {
            vs=bfonts[fnt]->maskVSize;
            for(int j=0;j<vs;j++)
            {
            ln<<=8;
            ln|=bfonts[fnt]->bitChar[c]->bitmask[j+k*vs];
            }
        LCD128x64_setVByteLine(x+k,y,bfonts[fnt]->height,ln);
        ln=0;
        }
        LCD128x64_setVByteLine(x+k,y,bfonts[fnt]->height,sp);
        break;
case TS_White_Black:
        /*for (k=0;k<fonts[fnt]->gChars[c].width;k++)
        LCD128x64_setVByteLine(x+k,y,fonts[fnt]->height,~fonts[fnt]->gChars[c].bitmask[k]);
        LCD128x64_setVByteLine(x+k,y,fonts[fnt]->height,~sp);*/
        break;

case TS_XOR:
    for (k=0;k<bfonts[fnt]->bitChar[c]->width;k++)
    {
        vs=bfonts[fnt]->maskVSize;
        for(int j=0;j<vs;j++)
        {
        ln<<=8;
        ln|=bfonts[fnt]->bitChar[c]->bitmask[j+k*vs];
        }
    LCD128x64_xorVByteLine(x+k,y,bfonts[fnt]->height,ln);
    ln=0;
    }
    break;
case TS_Black_Transparent:
    for (k=0;k<bfonts[fnt]->bitChar[c]->width;k++)
    {
        vs=bfonts[fnt]->maskVSize;
        for(int j=0;j<vs;j++)
        {
        ln<<=8;
        ln|=bfonts[fnt]->bitChar[c]->bitmask[j+k*vs];
        }
    LCD128x64_putVByteLine(x+k,y,bfonts[fnt]->height,ln);
    ln=0;
    }

    break;
case TS_White_Transparent:
    for (k=0;k<bfonts[fnt]->bitChar[c]->width;k++)
    {
        vs=bfonts[fnt]->maskVSize;
        for(int j=0;j<vs;j++)
        {
        ln<<=8;
        ln|=bfonts[fnt]->bitChar[c]->bitmask[j+k*vs];
        }
    LCD128x64_clearVByteLine(x+k,y,bfonts[fnt]->height,ln);
    ln=0;
    }  break;
}
k++;
return k;
}

uint16_t TextOut_printString(uint8_t x,uint8_t y,TextStyle style,uint8_t fnt, char* str)
{
if (str==0) return 0;
    int rx,i=0;
rx=x;
while (str[i]!=0)
{
rx+=TextOut_printChar(rx,y, style,fnt,str[i]);
i++;
}
return rx;
}

uint16_t TextOut_getStringWidth(uint8_t fnt, char* str)
{
    if (str==0) return 0;
	int rx,i=0;
	unsigned char c;
	rx=0;
	c=str[i];

	while (c!=0)
	{

        if(bfonts[fnt]->bitChar[c]!=0)rx+=bfonts[fnt]->bitChar[c]->width+1;
        i++;
        c=str[i];
	}
    return rx-1;
}

uint16_t TextOut_getStringHeight(uint8_t fnt)
{
    return bfonts[fnt]->height;
}

uint16_t TextOut_printStringAlign(int16_t x,int16_t y,int16_t w,int16_t h,TextStyle style,TextAlign align,uint8_t fnt, char* str)
{
    if (str==0) return 0;
    int16_t tx=0,ty,tw,th;

	tw=TextOut_getStringWidth(fnt,str);
	th=TextOut_getStringHeight(fnt);
    ty=(h-th)/2;
    if (align==Txt_Align_Center)
			{
            tx=(w-tw)/2;
			if (tx<1) tx=1;
			}
    if (align==Txt_Align_Right)
			{
            tx=w-tw;
			}
    if (align==Txt_Align_Left)
			{
            tx=0;
			}
return	TextOut_printString(x+tx,y+ty,style,fnt,str);

}

int16_t int2str(int i,char* buf,int16_t sz)//���������� ������ ����� � ������
{
    char tbuf[10];
    uint8_t ln=0,tl=0,k=0;
    int tmp1;
    if(sz<=0) return 0;
    if (i<0) {buf[0]='-';ln++;i=-i;}
    tmp1=i;
    do
    {
        tbuf[k]=tmp1%10;
        tmp1/=10;
        k++;
    } while ((k<10)&&(tmp1>0));
    tl=k-1;
    if (k>sz) k=sz;
    for(int8_t j = 0; j<k; j++)
        {
        buf[ln]=tbuf[tl]+'0';
        ln++;
        tl--;
        }
    buf[ln]=0;
    return ln;
}

int16_t float2str(float f,char* buf,int16_t sz,uint8_t prec,uint8_t zeros)//buf size>sz!!
{
    if(sz<=0) return 0;
    static float fprec[6]={1,10,100,1000,10000,100000};
    int32_t tmp;
    int16_t l1=0,tl,k=0,z=prec;
    char tbuf[10];
    if (f<0) {buf[l1]='-';l1++;f=-f;}
    else
    {buf[l1]=' ';l1++;}
    //ln++;
    tmp=(int32_t)(f*fprec[prec]+0.5);
    k=tmp%10;
    while ((k==0)&&(z>1))
    {
        z--;
        tmp/=10;
        k=tmp%10;
    }
    k=0;
    do
    {
        if(z==0) {tbuf[k]='.';k++;}
        tbuf[k]=tmp%10+'0';
        k++;
        z--;
        tmp/=10;


    } while ((k<10)&&((tmp>0)||(z>=0)));
    tl=k;
    k=0;
    while ((k<sz)&&(tl>0))
    {
        tl--;
        buf[l1]=tbuf[tl];
        if ((buf[l1])!='.') k++;
        l1++;
    }
    //if (buf[l1-1] == '.') l1--;
    buf[l1]=0;
    return (l1);
}

/*int16_t float2str(float f,char* buf,int16_t sz,int8_t zeros)//buf size>sz!!
{
    static float prec[6]={1,10,100,1000,10000,100000};
    if(sz<=0) return 0;
    int32_t tmp1,tmp0;
    int16_t l1=0,ln=sz,tl;
    if (f<0) {buf[0]='-';l1++;f=-f;}
    else
    {buf[0]=' ';l1++;}
    ln--;
    tmp1=(int32_t)(f);

    tl = int2str(tmp1,&buf[l1],ln);
    ln-=tl;
    l1+=tl;
    if(ln<=0) return l1;
    buf[l1]='.';
    l1++;
    ln%=6;
    tmp0=(int32_t)(f*prec[ln]+0.5)-tmp1*prec[ln];
    while ((tmp0>0)&&(tmp0%10==0)) {tmp0/=10;ln--;}


    tl = int2str(tmp0,&buf[l1],ln);
    ln-=tl;
    l1+=tl;
    if ((ln<=0)||(!zeros)) return l1;
    for(;ln>0;ln--)
    {
        buf[l1]='0';
        l1++;
    }
    buf[l1]=0;
    return (l1);
}*/


