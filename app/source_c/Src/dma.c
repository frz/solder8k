/**
  ******************************************************************************
  * File Name          : SPI.c
  * Description        : This file provides code for the configuration
  *                      of the SPI instances.
  ******************************************************************************
  *
  * COPYRIGHT(c) 2017 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "spi.h"
#include "stm32f3xx_ll_dma.h"
#include "nvic.h"

//DMA_InitTypeDef DMA_InitStructW,DMA_InitStructR;
//char tmp,buf[16];
//UniData uData[2];
uint8_t TransferComplete=0;
void DMA_init()
{

    __HAL_RCC_DMA2_CLK_ENABLE();

    DMA2->IFCR=0x0FFFFF;
    LL_DMA_SetMemoryAddress(DMA2,LL_DMA_CHANNEL_1,0);
    LL_DMA_SetChannelPriorityLevel(DMA2,LL_DMA_CHANNEL_1,LL_DMA_PRIORITY_HIGH);

    LL_DMA_SetDataTransferDirection(DMA2,LL_DMA_CHANNEL_1,LL_DMA_DIRECTION_PERIPH_TO_MEMORY);
    LL_DMA_SetMemoryIncMode(DMA2,LL_DMA_CHANNEL_1,LL_DMA_MEMORY_INCREMENT);
    LL_DMA_SetMemorySize(DMA2,LL_DMA_CHANNEL_1,LL_DMA_MDATAALIGN_BYTE);
    LL_DMA_SetMode(DMA2,LL_DMA_CHANNEL_1,LL_DMA_MODE_NORMAL);

    LL_DMA_SetPeriphAddress(DMA2,LL_DMA_CHANNEL_1,LL_SPI_DMA_GetRegAddr(SPI3));
    LL_DMA_SetPeriphIncMode(DMA2,LL_DMA_CHANNEL_1,LL_DMA_PERIPH_NOINCREMENT);
    LL_DMA_SetPeriphSize(DMA2,LL_DMA_CHANNEL_1,LL_DMA_PDATAALIGN_BYTE);

    LL_DMA_SetDataLength(DMA2,LL_DMA_CHANNEL_1,0);
    LL_DMA_EnableIT_TC(DMA2,LL_DMA_CHANNEL_1);
    //NVIC_DMA2_interrupt_enable();


    LL_DMA_SetMemoryAddress(DMA2,LL_DMA_CHANNEL_2,0);
    LL_DMA_SetChannelPriorityLevel(DMA2,LL_DMA_CHANNEL_2,LL_DMA_PRIORITY_LOW);

    LL_DMA_SetDataTransferDirection(DMA2,LL_DMA_CHANNEL_2,LL_DMA_DIRECTION_MEMORY_TO_PERIPH);
    LL_DMA_SetMemoryIncMode(DMA2,LL_DMA_CHANNEL_2,LL_DMA_MEMORY_INCREMENT);
    LL_DMA_SetMemorySize(DMA2,LL_DMA_CHANNEL_2,LL_DMA_MDATAALIGN_BYTE);
    LL_DMA_SetMode(DMA2,LL_DMA_CHANNEL_2,LL_DMA_MODE_NORMAL);

    LL_DMA_SetPeriphAddress(DMA2,LL_DMA_CHANNEL_2,LL_SPI_DMA_GetRegAddr(SPI3));
    LL_DMA_SetPeriphIncMode(DMA2,LL_DMA_CHANNEL_2,LL_DMA_PERIPH_NOINCREMENT);
    LL_DMA_SetPeriphSize(DMA2,LL_DMA_CHANNEL_2,LL_DMA_PDATAALIGN_BYTE);

    LL_DMA_SetDataLength(DMA2,LL_DMA_CHANNEL_2,0);
    LL_DMA_EnableIT_TC(DMA2,LL_DMA_CHANNEL_2);
   NVIC_DMA2_interrupt_enable();
   //LL_SPI_EnableDMAReq_TX(SPI3);
   //LL_SPI_EnableDMAReq_RX(SPI3);
}

void DMA2_Channel1_IRQHandler(void)//DMA SPI RX INTERRUPT
{

TransferComplete=1;
    LL_SPI_DisableDMAReq_TX(SPI3);
    LL_SPI_DisableDMAReq_RX(SPI3);
    while (LL_SPI_IsActiveFlag_BSY(SPI3));
   LL_DMA_DisableChannel(DMA2,LL_DMA_CHANNEL_1);
   LL_DMA_DisableChannel(DMA2,LL_DMA_CHANNEL_2);
    LL_DMA_ClearFlag_TC1(DMA2);
}

void DMA2_Channel2_IRQHandler(void)//DMA SPI TX INTERRUPT
{


    LL_DMA_ClearFlag_TC2(DMA2);

}
uint32_t dma_tmp=0;
void DMA_SPI_RX(char* data, uint32_t sz)
{
    LL_DMA_DisableChannel(DMA2,LL_DMA_CHANNEL_1);
    LL_DMA_DisableChannel(DMA2,LL_DMA_CHANNEL_2);

    TransferComplete=0;
    LL_DMA_SetDataLength(DMA2,LL_DMA_CHANNEL_2,sz);
    LL_DMA_SetMemoryIncMode(DMA2,LL_DMA_CHANNEL_2,LL_DMA_MEMORY_NOINCREMENT);
    LL_DMA_SetMemoryAddress(DMA2,LL_DMA_CHANNEL_2,(uint32_t)&dma_tmp);

    LL_DMA_SetDataLength(DMA2,LL_DMA_CHANNEL_1,sz);
    LL_DMA_SetMemoryIncMode(DMA2,LL_DMA_CHANNEL_1,LL_DMA_MEMORY_INCREMENT);
    LL_DMA_SetMemoryAddress(DMA2,LL_DMA_CHANNEL_1,(uint32_t) data);

    LL_SPI_EnableDMAReq_TX(SPI3);
    LL_SPI_EnableDMAReq_RX(SPI3);
    LL_DMA_EnableChannel(DMA2,LL_DMA_CHANNEL_1);
    LL_DMA_EnableChannel(DMA2,LL_DMA_CHANNEL_2);

}

void DMA_SPI_TX(char* data, uint32_t sz)
{

    LL_DMA_DisableChannel(DMA2,LL_DMA_CHANNEL_1);
    LL_DMA_DisableChannel(DMA2,LL_DMA_CHANNEL_2);

    TransferComplete=0;
    LL_DMA_SetDataLength(DMA2,LL_DMA_CHANNEL_2,sz);
    LL_DMA_SetMemoryIncMode(DMA2,LL_DMA_CHANNEL_2,LL_DMA_MEMORY_INCREMENT);
    LL_DMA_SetMemoryAddress(DMA2,LL_DMA_CHANNEL_2,(uint32_t)data);

    LL_DMA_SetDataLength(DMA2,LL_DMA_CHANNEL_1,sz);
    LL_DMA_SetMemoryIncMode(DMA2,LL_DMA_CHANNEL_1,LL_DMA_MEMORY_NOINCREMENT);
    LL_DMA_SetMemoryAddress(DMA2,LL_DMA_CHANNEL_1,(uint32_t)&dma_tmp);

    LL_SPI_EnableDMAReq_TX(SPI3);
    LL_SPI_EnableDMAReq_RX(SPI3);
    LL_DMA_EnableChannel(DMA2,LL_DMA_CHANNEL_1);
    LL_DMA_EnableChannel(DMA2,LL_DMA_CHANNEL_2);

}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
