#ifndef __LCD128x64_H
#define __LCD128x64_H
#ifdef __cplusplus
 extern "C" {
#endif
#include "stm32f3xx_hal.h"

#define 	PIN_LCD_CS1 	GPIO_PIN_3	//portB
#define 	PIN_LCD_CS2 	GPIO_PIN_4	//portB
#define 	PIN_LCD_RW 		GPIO_PIN_5	//portB
#define 	PIN_LCD_RST 	GPIO_PIN_6	//portB
#define 	PIN_LCD_E 		GPIO_PIN_7	//portB
#define 	PIN_LCD_DI 		GPIO_PIN_8	//portB
#define 	PIN_LCD_LED 	GPIO_PIN_1	//portC



typedef struct
{
	uint8_t VByteMap[128][8];
} VByteMem;

typedef struct
{
	uint8_t	enable;
	uint8_t	curX;
	uint8_t	curP;

	uint8_t	curY;
	uint64_t vMem[128];
	VByteMem *vByteMem;
} LCD_Data;


void LCD128x64_init(void);
void LCD128x64_proc(void);
void LCD128x64_putVByteLine(uint8_t x,uint8_t y,uint8_t l,uint64_t bl);
void LCD128x64_setVByteLine(uint8_t x,uint8_t y,uint8_t l,uint64_t bl);
void LCD128x64_clearVByteLine(uint8_t x,uint8_t y,uint8_t l,uint64_t bl);
void LCD128x64_xorVByteLine(uint8_t x,uint8_t y,uint8_t l,uint64_t bl);
void LCD128x64_writeVMem2LCD();
void LCD128x64_enable();
#ifdef __cplusplus
}
#endif
#endif
