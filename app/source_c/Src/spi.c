/**
  ******************************************************************************
  * File Name          : SPI.c
  * Description        : This file provides code for the configuration
  *                      of the SPI instances.
  ******************************************************************************
  *
  * COPYRIGHT(c) 2017 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "spi.h"
#include "stm32f3xx_ll_spi.h"
//#include "stm32f3xx_hal.h"
#include "gpio.h"

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

//LL_SPI_InitTypeDef hspi1;
LL_SPI_InitTypeDef initSpi;

/* SPI1 init function */



uint8_t SPI3_Write(uint8_t data)
{
uint8_t tmp;
LL_SPI_TransmitData8(SPI3,data);
while (!LL_SPI_IsActiveFlag_RXNE(SPI3));
tmp=LL_SPI_ReceiveData8(SPI3);
return tmp;
}

uint8_t SPI3_Read()
{
uint16_t s1;
LL_SPI_TransmitData8(SPI3,0);
while (!LL_SPI_IsActiveFlag_RXNE(SPI3));
s1=LL_SPI_ReceiveData8(SPI3);
return s1;
}


void SPI1_Init(void)
{

    __HAL_RCC_SPI1_CLK_ENABLE();

}



void SPI1_PinInit()
{
      GPIO_InitTypeDef GPIO_InitStruct;
    /**SPI1 GPIO Configuration
    PC6     ------> SPI1_NSS
    PC7     ------> SPI1_SCK
    PC8     ------> SPI1_MISO
    PC9     ------> SPI1_MOSI
    */
    GPIO_InitStruct.Pin = GPIO_PIN_6|GPIO_PIN_7|GPIO_PIN_8|GPIO_PIN_9;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF5_SPI1;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
}

void SPI3_PinInit()
{

 GPIO_InitTypeDef GPIO_InitStruct;


  /**SPI3 GPIO Configuration
  PA15     ------> SPI3_NSS
  PC10     ------> SPI3_SCK
  PC11     ------> SPI3_MISO
  PC12     ------> SPI3_MOSI
  */
  HAL_GPIO_WritePin(GPIOA,GPIO_PIN_15,GPIO_PIN_SET);
  GPIO_InitStruct.Pin = GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
 // GPIO_InitStruct.Alternate = GPIO_AF6_SPI3;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  GPIO_InitStruct.Pin = GPIO_PIN_10|GPIO_PIN_11|GPIO_PIN_12;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF6_SPI3;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

}

/* SPI3 init function */
void SPI3_Init(void)
{
    __HAL_RCC_SPI3_FORCE_RESET();
    __HAL_RCC_SPI3_RELEASE_RESET();

     __HAL_RCC_SPI3_CLK_ENABLE();


     SPI3_PinInit();
     LL_SPI_SetBaudRatePrescaler(SPI3,LL_SPI_BAUDRATEPRESCALER_DIV2);
    LL_SPI_SetTransferBitOrder(SPI3,LL_SPI_MSB_FIRST);
    LL_SPI_SetClockPhase(SPI3,LL_SPI_PHASE_1EDGE);
   LL_SPI_SetRxFIFOThreshold(SPI3,LL_SPI_RX_FIFO_TH_QUARTER);
    LL_SPI_DisableCRC(SPI3);
    LL_SPI_SetDataWidth(SPI3,LL_SPI_DATAWIDTH_8BIT);
    LL_SPI_SetClockPolarity(SPI3,LL_SPI_POLARITY_LOW);

    LL_SPI_SetNSSMode(SPI3,LL_SPI_NSS_SOFT);
    LL_SPI_SetMode(SPI3,LL_SPI_MODE_MASTER);
    LL_SPI_SetTransferDirection(SPI3,LL_SPI_FULL_DUPLEX);
   // LL_SPI_DisableDMAReq_TX(SPI3);
   // LL_SPI_DisableDMAReq_RX(SPI3);
   // LL_SPI_EnableDMAReq_TX(SPI3);
   // LL_SPI_EnableDMAReq_RX(SPI3);
LL_SPI_Enable(SPI3);

//LL_SPI_GetDMAParity_RX()
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
