#include "spiflash.h"

#include "gpio.h"
#include "spi.h"
#include "dma.h"
/*typedef struct
{
    uint8_t cmd;

}AddrRW;*/
extern uint8_t TransferComplete;
__STATIC_INLINE void SPIFlash_select()
{
    SPI3_WaitTransmit();
    GPIOA->BRR= (uint32_t)GPIO_PIN_15;
}
__STATIC_INLINE void SPIFlash_unselect()
{
    SPI3_WaitTransmit();
    GPIOA->BSRR= (uint32_t)GPIO_PIN_15;
}
void SPIFlash_wakeUP()
{
    uint32_t id;
     //   SPIFlash_unselect();
    SPIFlash_select();
    SPI3_Write(0xab);
    SPIFlash_unselect();
     id=SPIFlash_readId();
     id=SPIFlash_readId();
     id=SPIFlash_readId();
     id=SPIFlash_readId();
     id=SPIFlash_readId();
     id=SPIFlash_readId();

    SPIFlash_unselect();
      //UNSELECT_FLASH;
}



uint32_t SPIFlash_readId()
{
    uint32_t id=0;
  SPIFlash_select();
  SPI3_Write(SPIFLASH_IDREAD);
 id = SPI3_Read();
  id<<=8;
  id |= SPI3_Read();
 id<<=8;
  id |= SPI3_Read();

  SPIFlash_unselect();
  return id;
}
uint8_t SPIFlash_isBusy()
{
    uint8_t s;
    SPIFlash_select();
    SPI3_Write(SPIFLASH_STATUSREAD);
   s = SPI3_Read();
   SPIFlash_unselect();
   return (s&1);
}

uint8_t SPIFlash_readStatus()
{
    uint8_t s;
  SPIFlash_select();
  SPI3_Write(SPIFLASH_STATUSREAD);
 s = SPI3_Read();
  SPIFlash_unselect();
  return s;
}
uint32_t tmp;

uint8_t SPIFlash_readByte(uint32_t addr)
{
    SPIFlash_select();
    tmp=(addr<<16)+(addr&0xff00)+(addr>>16);
    tmp=(tmp<<8)+SPIFLASH_READ;
    DMA_SPI_TX(&tmp,4);
    uint8_t s;
    while(TransferComplete==0){};
    s = SPI3_Read();
    SPIFlash_unselect();
  return s;
}

int32_t SPIFlash_readData(uint32_t addr,uint32_t size, uint8_t * data)
{
    SPIFlash_select();
    tmp=(addr<<16)+(addr&0xff00)+(addr>>16);
    tmp=(tmp<<8)+SPIFLASH_READ;
    DMA_SPI_TX(&tmp,4);
    uint8_t s;
    while(TransferComplete==0){};
    DMA_SPI_RX(data,size);
    while(TransferComplete==0){};
    SPIFlash_unselect();
  return 0;
}

int32_t SPIFlash_readDataSect(uint32_t sect,uint8_t *data)
{
    if (sect>1023) return -1;
    DataInf *inf;
    uint32_t addr;
    inf=(DataInf*)data;
    addr=sect<<12;
    if (SPIFlash_readByte(addr)!=inf->key) return -1;
    SPIFlash_readData(addr,inf->size,data);
    return inf->size;
}

void SPIFlash_writeEnable()
{
    SPIFlash_select();
    SPI3_Write(SPIFLASH_WRITEENABLE);
    SPIFlash_unselect();
}


void SPIFlash_writeByte(uint32_t addr,uint8_t data)
{
    SPIFlash_writeEnable();
    SPIFlash_select();
    tmp=(addr<<16)+(addr&0xff00)+(addr>>16);
    tmp=(tmp<<8)+SPIFLASH_WRITE;
    DMA_SPI_TX(&tmp,4);
    while(TransferComplete==0){};
    SPI3_Write(data);
    SPIFlash_unselect();
}

void SPIFlash_writePage(uint32_t addr,uint32_t size,uint8_t * data)
{
    SPIFlash_writeEnable();
    SPIFlash_select();
    tmp=(addr<<16)+(addr&0xff00)+(addr>>16);
    tmp=(tmp<<8)+SPIFLASH_WRITE;
    DMA_SPI_TX(&tmp,4);
    while(TransferComplete==0){};
    DMA_SPI_TX(data,size);
    while(TransferComplete==0){};
    SPIFlash_unselect();
}

void SPIFlash_writeDataSect(uint32_t sect,uint8_t * data)
{
    if (sect>1023) return -1;
    DataInf *inf;
    uint32_t addr,byte2tx,txsz,txcomp=0;
    inf=(DataInf*)data;

    addr=sect<<12;
    inf->size&=0xFFF;
    byte2tx=inf->size;

    while (byte2tx>0)
    {
    txsz=256;
    if (byte2tx<txsz) txsz=byte2tx;
    byte2tx-=txsz;
    while (SPIFlash_isBusy());
    SPIFlash_writePage(addr+txcomp,txsz,(uint8_t *)((uint32_t)data+txcomp));
    txcomp+=txsz;
    }
    while (SPIFlash_isBusy());
}

void SPIFlash_eraseSect(uint32_t sect)
{
    if (sect>1023) return;
    uint32_t addr;
    addr=sect<<12;

    while (SPIFlash_isBusy());

    SPIFlash_writeEnable();
    SPIFlash_select();
    tmp=(addr&0xf000)+(addr>>16);
    tmp=(tmp<<8)+SPIFLASH_ERASE_4K;
    DMA_SPI_TX(&tmp,4);
    while(TransferComplete==0){};
    SPIFlash_unselect();
    while (SPIFlash_isBusy());
}
