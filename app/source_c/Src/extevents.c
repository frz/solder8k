#include "extevents.h"

uint32_t keysState;

KeyState keyState;
ExtEvents extEvents;

#define Pin_Key_Down GPIO_PIN_11	//PA11
#define Pin_Key_Left GPIO_PIN_12	//PA12

#define Pin_Key_Right GPIO_PIN_13	//PD13
#define Pin_Key_Enter GPIO_PIN_12	//PD12

#define Pin_Key_Light GPIO_PIN_0	//PE0
#define Pin_Key_Cancel GPIO_PIN_5	//PE5

#define Pin_Key_Up GPIO_PIN_6       //PF6



void ExtEvents_init()
{

    keyState.KeyUp=KEY_NOT_PRESSED;
    keyState.KeyDown=KEY_NOT_PRESSED;
    keyState.KeyRight=KEY_NOT_PRESSED;
    keyState.KeyLeft=KEY_NOT_PRESSED;
    keyState.KeyEnter=KEY_NOT_PRESSED;
    keyState.KeyCancel=KEY_NOT_PRESSED;
    keyState.KeyLight=KEY_NOT_PRESSED;
    extEvents.KeyUpClicked.keyCode=KeyUp;
    extEvents.KeyDownClicked.keyCode=KeyDown;
    extEvents.KeyLeftClicked.keyCode=KeyLeft;
    extEvents.KeyRightClicked.keyCode=KeyRight;
    extEvents.KeyEnterClicked.keyCode=KeyEnter;
    extEvents.KeyCancelClicked.keyCode=KeyCancel;
    extEvents.KeyLightClicked.keyCode=KeyLight;


}
void ExtEvents_keyProc()
{
	static uint8_t rpt=0;
	//------------external Keys pin get state(with filter)-----------------------------------
    keyState.KeyDown<<=1;
    keyState.KeyDown|=HAL_GPIO_ReadPin(GPIOA,Pin_Key_Down);
    keyState.KeyLeft<<=1;
    keyState.KeyLeft|=HAL_GPIO_ReadPin(GPIOA,Pin_Key_Left);
    keyState.KeyRight<<=1;
    keyState.KeyRight|=HAL_GPIO_ReadPin(GPIOD,Pin_Key_Right);
    keyState.KeyEnter<<=1;
    keyState.KeyEnter|=HAL_GPIO_ReadPin(GPIOD,Pin_Key_Enter);
    keyState.KeyLight<<=1;
    keyState.KeyLight|=HAL_GPIO_ReadPin(GPIOE,Pin_Key_Light);
    keyState.KeyCancel<<=1;
    keyState.KeyCancel|=HAL_GPIO_ReadPin(GPIOE,Pin_Key_Cancel);
    keyState.KeyUp<<=1;
    keyState.KeyUp|=HAL_GPIO_ReadPin(GPIOF,Pin_Key_Up);
	rpt++;
	if (rpt>=10)
	{
		rpt=0;
		ExtEvents_generateEventsProc();
	}
}


void ExtEvents_generateKeyEvent(uint8_t keyState,Event *event)
{
	if (keyState==KEY_PRESSED)
		{
		if (event->repeatTime<2)
			{
			event->repeatTime=KEY_FIRST_REPEAT-event->repeatTime*KEY_NEXT_REPEAT_DEC;
            //event->triggered=1;
            keysState|=event->keyCode;
			}
		event->repeatTime-=1;
		};
	if (keyState==KEY_NOT_PRESSED) event->repeatTime=0;
}


void ExtEvents_generateEventsProc()
{

ExtEvents_generateKeyEvent(keyState.KeyUp,&extEvents.KeyUpClicked);
ExtEvents_generateKeyEvent(keyState.KeyDown,&extEvents.KeyDownClicked);
ExtEvents_generateKeyEvent(keyState.KeyLeft,&extEvents.KeyLeftClicked);
ExtEvents_generateKeyEvent(keyState.KeyRight,&extEvents.KeyRightClicked);
ExtEvents_generateKeyEvent(keyState.KeyEnter,&extEvents.KeyEnterClicked);
ExtEvents_generateKeyEvent(keyState.KeyCancel,&extEvents.KeyCancelClicked);
ExtEvents_generateKeyEvent(keyState.KeyLight,&extEvents.KeyLightClicked);

};


