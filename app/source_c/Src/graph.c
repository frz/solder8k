#include "graph.h"
#include "lcd128x64.h"

void Graph_rect(uint8_t x,uint8_t y,uint8_t w,uint8_t h,Rect_Style style)
{
	int k=0;
	uint64_t ln=~0,ln2;
	//h=height;
	ln2=((uint64_t)1<<(h-1))+1;

	switch (style)
	{
	case RS_Black_Black:
			for (k=x;k<x+w;k++)
			LCD128x64_setVByteLine(k,y,h,ln);
			break;
	case RS_Black_White:
			LCD128x64_setVByteLine(x,y,h,ln);
			LCD128x64_setVByteLine(x+w-1,y,h,ln);
			for (k=x+1;k<x+w-1;k++)
			LCD128x64_setVByteLine(k,y,h,ln2);
			break;
	case RS_White_White:
            for (k=x;k<x+w;k++)
            {
                LCD128x64_setVByteLine(k,y,h,~ln);
            }
			break;
	case RS_Black_Transparent:
			LCD128x64_setVByteLine(x,y,h,ln);
			LCD128x64_setVByteLine(x+w-1,y,h,ln);
			for (k=x+1;k<(x+w-1);k++)
			LCD128x64_putVByteLine(k,y,h,ln2);
			break;
	case RS_White_Transparent:
			LCD128x64_clearVByteLine(x,y,h,ln);
			LCD128x64_clearVByteLine(x+w-1,y,h,ln);
			for (k=x+1;k<(x+w-1);k++)
			LCD128x64_clearVByteLine(k,y,h,ln2);
			break;
	case RS_Dotted:
			ln2=0xAAAAAAAAAAAAAAAA;
			for (k=x;k<(x+w);k++)
			{
			LCD128x64_setVByteLine(k,y,h,ln2);
			ln2=~ln2;
			}
			break;
	}

}

void Graph_Hline(uint8_t x, uint8_t y, uint8_t len, Line_Style style)
{
	uint8_t k = 0,maxx=len+x;
	switch (style)
	{

	case LS_Black:
		for (k = x; k<(maxx); k++){
			LCD128x64_setVByteLine(k, y, 1,(uint64_t)1);}
		break;

	case LS_White:
		for (k = x; k<(x + len); k++)
			LCD128x64_clearVByteLine(k, y, 1, (uint64_t)1);
		break;
	case LS_Dotted0:
		for (k = x; k<(x + len); k++)
			LCD128x64_setVByteLine(k, y, 1, (uint64_t)(k & 1));
		break;
	case LS_Dotted1:
		for (k = x; k<(x + len); k++)
			LCD128x64_setVByteLine(k, y, 1, (uint64_t)((k + 1) & 1));
		break;

	}
}
void Graph_Vline(uint8_t x,uint8_t y,uint8_t len,Line_Style style)
{
			uint64_t ln=0,ln2=0xAAAAAAAAAAAAAAAA;
			switch (style)
			{
			case LS_Black:
					LCD128x64_setVByteLine(x,y,len,~ln);
					break;
			case LS_White:
					LCD128x64_clearVByteLine(x,y,len,~ln);
					break;
			case LS_Dotted0:
					LCD128x64_setVByteLine(x,y,len,ln2);
					break;
			case LS_Dotted1:
					LCD128x64_setVByteLine(x,y,len,~ln2);
					break;
			}
}
