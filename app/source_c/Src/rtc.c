#include  "rtc.h"

//TIM_HandleTypeDef tim4Handle;
#include "stm32f3xx_hal_rtc.h"

//��������� ��� sys clk=16MHz
RTC_HandleTypeDef rtcHandle;

RTC_TimeTypeDef rtcTime;
RTC_DateTypeDef rtcDate;

RTime currentTime;
RDate currentDate;

void RTC_init()
{
    rtcHandle.Instance=RTC;
    HAL_PWR_EnableBkUpAccess();
    if ((rtcHandle.Instance->DR==0x2101)&&(rtcHandle.Instance->TR == 0))

    {
        __HAL_RCC_BACKUPRESET_FORCE();
        __HAL_RCC_BACKUPRESET_RELEASE();
        __HAL_RCC_LSE_CONFIG(RCC_LSE_ON);
        uint32_t tickstart = HAL_GetTick();

        while(__HAL_RCC_GET_FLAG(RCC_FLAG_LSERDY) == RESET)
        {
            if((HAL_GetTick() - tickstart) > 5000)
            {
                return ;
            }
        }
        __HAL_RCC_RTC_CONFIG(RCC_RTCCLKSOURCE_LSE);
        __HAL_RCC_PWR_CLK_ENABLE();
        __HAL_RCC_RTC_ENABLE();

        rtcHandle.Init.HourFormat=RTC_HOURFORMAT_24;
        rtcHandle.Init.SynchPrediv = 255;
        rtcHandle.Init.AsynchPrediv = 127;
        HAL_RTC_Init(&rtcHandle);
    }
}

void RTC_proc()
{
    HAL_RTC_GetTime(&rtcHandle,&rtcTime,0);
    currentTime.hh=rtcTime.Hours;
    currentTime.mm=rtcTime.Minutes;
    currentTime.ss=rtcTime.Seconds;
    HAL_RTC_GetDate(&rtcHandle,&rtcDate,0);
    currentDate.dd=rtcDate.Date;
    currentDate.yy=rtcDate.Year;
    currentDate.mm=rtcDate.Month;
}

void RTC_getDateTime(RTime *rTime, RDate *rDate)
{

    if (rTime!=0)
    {
    HAL_RTC_GetTime(&rtcHandle,&rtcTime,0);
    rTime->hh=rtcTime.Hours;
    rTime->mm=rtcTime.Minutes;
    rTime->ss=rtcTime.Seconds;
    }
    if (rDate!=0)
    {
    HAL_RTC_GetDate(&rtcHandle,&rtcDate,0);
    rDate->dd=rtcDate.Date;
    rDate->yy=rtcDate.Year;
    rDate->mm=rtcDate.Month;
    }
}

void RTC_setDateTime(RTime *rTime, RDate *rDate)
{

    if (rTime!=0)
    {
        rtcTime.Hours=rTime->hh;
        rtcTime.Minutes=rTime->mm;
        rtcTime.Seconds=rTime->ss;
        HAL_RTC_SetTime(&rtcHandle,&rtcTime,0);
    }
    if (rDate!=0)
    {
        rtcDate.Date=rDate->dd;
        rtcDate.Year=rDate->yy;
        rtcDate.Month=rDate->mm;
        HAL_RTC_SetDate(&rtcHandle,&rtcDate,0);
    }
}

void HAL_RTC_MspInit(RTC_HandleTypeDef* hrtc)
{
 /*   GPIO_InitStruct.Pin = GPIO_PIN_14|GPIO_PIN_15;
    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);*/
}
