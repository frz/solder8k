#ifndef EXTEVENTS_H
#define EXTEVENTS_H
#ifdef __cplusplus
 extern "C" {
#endif
 #include "stm32f3xx_hal.h"

#define KEY_PRESSED 0x0
#define KEY_NOT_PRESSED 0xff
#define KEY_FIRST_REPEAT 50
#define KEY_NEXT_REPEAT_DEC 40


typedef enum
 {
    KeyUp=1,
    KeyDown=2,
    KeyRight=4,
    KeyLeft=8,
    KeyEnter=16,
    KeyCancel=32,
    KeyLight=64
 }
 KeyCode;

typedef struct
{
uint8_t triggered;
uint16_t repeatTime;
uint32_t keyCode;
} Event;

typedef struct
{
Event KeyUpClicked;
Event KeyDownClicked;
Event KeyRightClicked;
Event KeyLeftClicked;
Event KeyEnterClicked;
Event KeyCancelClicked;
Event KeyLightClicked;
} ExtEvents;

typedef struct
{
uint8_t KeyUp;
uint8_t KeyDown;
uint8_t KeyRight;
uint8_t KeyLeft;
uint8_t KeyEnter;
uint8_t KeyCancel;
uint8_t KeyLight;
} KeyState;

void ExtEvents_init();
void ExtEvents_keyProc();
void ExtEvents_generateEventsProc();
#ifdef __cplusplus
 }
#endif
#endif
