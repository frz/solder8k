#include "lcd128x64.h"
#include "stm32f3xx_hal.h"


LCD_Data lcdData;


void LCD128x64_init()
{
//--------------------Video memory init----------------------
    lcdData.vByteMem=(VByteMem*)&lcdData.vMem;
	lcdData.curX=0;
	lcdData.curP=0;

	LCD128x64_enable();
}

/*void LCD128x64_getState()
{
    uint16_t tmp16;
    HAL_GPIO_WritePin(GPIOB,PIN_LCD_DI,GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOB,PIN_LCD_RW,GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOB,PIN_LCD_RW,GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOB,PIN_LCD_RW,GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOB,PIN_LCD_E,GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOB,PIN_LCD_E,GPIO_PIN_SET);
    tmp16=GPIOD->IDR;//GPIO_ReadInputData(GPIOD);
    HAL_GPIO_WritePin(GPIOB,PIN_LCD_E,GPIO_PIN_RESET);

}*/

void LCD128x64_setPage(uint8_t page)
{uint8_t tmp;
    HAL_GPIO_WritePin(GPIOB,PIN_LCD_DI,GPIO_PIN_RESET);

    HAL_GPIO_WritePin(GPIOB,PIN_LCD_RW,GPIO_PIN_RESET);

    tmp=0b10111000+page;
    //GPIO_Write(GPIOD,tmp);
    GPIOD->ODR=tmp;
    HAL_GPIO_WritePin(GPIOB,PIN_LCD_E,GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOB,PIN_LCD_E,GPIO_PIN_SET);

    HAL_GPIO_WritePin(GPIOB,PIN_LCD_E,GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOB,PIN_LCD_E,GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOB,PIN_LCD_E,GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOB,PIN_LCD_E,GPIO_PIN_RESET);

    //HAL_GPIO_WritePin(GPIOB,PIN_LCD_E);
}

void LCD128x64_setXpos(uint8_t x)
{uint8_t tmp;
    HAL_GPIO_WritePin(GPIOB,PIN_LCD_DI,GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOB,PIN_LCD_RW,GPIO_PIN_RESET);
    tmp=0b01000000+x;
    //GPIO_Write(GPIOD,tmp);
    GPIOD->ODR=tmp;
    HAL_GPIO_WritePin(GPIOB,PIN_LCD_E,GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOB,PIN_LCD_E,GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOB,PIN_LCD_E,GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOB,PIN_LCD_E,GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOB,PIN_LCD_E,GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOB,PIN_LCD_E,GPIO_PIN_RESET);

    //HAL_GPIO_WritePin(GPIOB,PIN_LCD_E);
}

void LCD128x64_selectChip(uint8_t chip)
{
    HAL_GPIO_WritePin(GPIOB, PIN_LCD_CS1,GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOB, PIN_LCD_CS2,GPIO_PIN_RESET);

    if (chip & 1) HAL_GPIO_WritePin(GPIOB, PIN_LCD_CS1,GPIO_PIN_SET);
    if (chip & 2) HAL_GPIO_WritePin(GPIOB, PIN_LCD_CS2,GPIO_PIN_SET);
}

void LCD128x64_enable()
{
	static uint8_t tmp;
	int k;
	LCD128x64_selectChip(3);
    HAL_GPIO_WritePin(GPIOB, PIN_LCD_RST,GPIO_PIN_RESET);
	for (k = 0; k < 300; k++);
    HAL_GPIO_WritePin(GPIOB, PIN_LCD_RST,GPIO_PIN_SET);
	for (k = 0; k < 300; k++);

    tmp = 0b00111111;
    HAL_GPIO_WritePin(GPIOB, PIN_LCD_RW,GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOB, PIN_LCD_DI,GPIO_PIN_RESET);
    //GPIO_Write(GPIOD, tmp);
    GPIOD->ODR=tmp;
    HAL_GPIO_WritePin(GPIOB, PIN_LCD_E,GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOB, PIN_LCD_E,GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOB, PIN_LCD_E,GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOB,PIN_LCD_E,GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOB,PIN_LCD_E,GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOB,PIN_LCD_E,GPIO_PIN_RESET);

	for (k = 0; k < 300; k++);

	LCD128x64_setPage(0);
	LCD128x64_setXpos(0);
}

void LCD128x64_writeVMem2LCD()
{
static uint8_t tmp,bank,page;
if (lcdData.curX>=64)
    {//HAL_GPIO_WritePin(GPIOC,PIN_LCD_LED);
		lcdData.curP++;
		lcdData.curP&=15;

		LCD128x64_setPage(lcdData.curP&7);
		LCD128x64_setXpos(0);
        lcdData.curX=0;//{HAL_GPIO_WritePin(GPIOC,PIN_LCD_LED);}
		return;
	}

bank=lcdData.curP>>3;//div by 8
page=lcdData.curP&7;// % 8
LCD128x64_selectChip(bank+1);
__disable_irq ();
tmp=lcdData.vByteMem->VByteMap[lcdData.curX+64*bank][page];
__enable_irq ();
    HAL_GPIO_WritePin(GPIOB,PIN_LCD_RW,GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOB,PIN_LCD_DI,GPIO_PIN_SET);
    GPIOD->ODR=tmp;
    HAL_GPIO_WritePin(GPIOB,PIN_LCD_E,GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOB,PIN_LCD_E,GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOB,PIN_LCD_E,GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOB,PIN_LCD_E,GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOB,PIN_LCD_E,GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOB,PIN_LCD_E,GPIO_PIN_RESET);

    lcdData.curX++;
}



void LCD128x64_putPix(uint8_t x,uint8_t y, uint8_t c)
{
    uint64_t c64;
	x&=127;
	c&=1;
	c64=((~c)&1)<<y;
//	__disable_irq ();
	lcdData.vMem[x]&=~c64;
	lcdData.vMem[x]|=c<<y;
//	__enable_irq ();
}

void LCD128x64_putVLine(uint8_t x,uint8_t y,uint8_t l, uint8_t c)
{uint64_t c64,l64;
	x&=127;
	c&=1;
	c64=c;
	c64--;

	l64=~c64;
	l64>>=(64-l);
	c64>>=(64-l);
	c64<<=y;

//	__disable_irq ();
	lcdData.vMem[x]|=l64<<y;
	lcdData.vMem[x]&=~c64;
//	__enable_irq ();


}

void LCD128x64_setVByteLine(uint8_t x,uint8_t y,uint8_t l,uint64_t bl)
{
    uint64_t c64;
	x&=127;
	//y&=63;
	//l&=63;
	c64=~0;
	c64<<=l;

	c64=~c64;
	bl&=c64;
	c64<<=y;
	bl<<=y;
//	__disable_irq ();
	lcdData.vMem[x]&=~c64;
	lcdData.vMem[x]|=bl;
//	__enable_irq ();
}

void LCD128x64_putVByteLine(uint8_t x,uint8_t y,uint8_t l,uint64_t bl)
{
    uint64_t c64;
	x&=127;
	c64=~0;
	c64<<=l;
	c64=~c64;
	bl&=c64;
	bl<<=y;
//	__disable_irq ();
	lcdData.vMem[x]|=bl;
//	__enable_irq ();
}

void LCD128x64_xorVByteLine(uint8_t x,uint8_t y,uint8_t l,uint64_t bl)
{
    uint64_t c64;
    x&=127;
    c64=~0;
    c64<<=l;
    c64=~c64;
    bl&=c64;
    bl<<=y;
//    __disable_irq ();
    lcdData.vMem[x]^=bl;
//    __enable_irq ();
}

void LCD128x64_clearVByteLine(uint8_t x,uint8_t y,uint8_t l,uint64_t bl)
{
    uint64_t c64;
	x&=127;
	c64=~0;
	c64<<=l;
	c64=~c64;
	c64&=bl;
	c64<<=y;
//	__disable_irq ();
	lcdData.vMem[x]&=~c64;
//	__enable_irq ();
}


void LCD128x64_proc()
{
LCD128x64_writeVMem2LCD();
}

